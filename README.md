# Marnoto

Service to manage users and so on


---

While you are creating code in your computer, you need to set some env variables, example:

```
DB_DATABASE=youclap-dev
DB_PASSWORD=youclap-sql-pw
DB_HOSTNAME=127.0.0.1
DB_USERNAME=root
DB_PORT=32768
ENDPOINT_TEATRO=https://teatro.api.dev.youclap.tech
```

You can also use program arguments `--hostname 0.0.0.0` to serve to all hosts
