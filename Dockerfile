FROM swift:5.1-slim

ENV ENVIRONMENT=prod

WORKDIR /app
COPY service-account.json /app
COPY marnoto /app
COPY .env /app
COPY .dev.env /app
COPY .prod.env /app

ENV HOSTNAME=0.0.0.0
ENV PORT=80
ENV GOOGLE_APPLICATION_CREDENTIALS=/app/service-account.json

EXPOSE $PORT

ENTRYPOINT ./marnoto serve --env $ENVIRONMENT --hostname $HOSTNAME --port $PORT
