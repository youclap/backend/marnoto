# CHANGELOG

<!--- next entry here -->

## 0.2.0
2019-09-18

### Features

- Upgrade shared helm chart (e7521e54fb44ce5720e82883cb6547d330b9e736)

## 0.1.0
2019-09-18

### Features

- **crud:** Apply CRUD Store to this service 🎉 (873fa7e7d4262b7a45f5ce8a4340f484802a161b)
- **backend-176:** Change healthcheck path (052d768a41179be1be238beb14a8181709cce99a)
- **backend-152:** Support profile (4802c234ceef78e2a366b40b73074fd32b848acb)
- **cors:** Configure CORS middleware (9ed0570727303acdbb0b116c2285dfb0eb9da091)
- **backend-182:** Create mexico endpoint (c7a920865e1d12ca597cbd793d3659e8df2dbca5)
- Profile for username (837a2e8028fbf6e821ba2a6c23e893b0a6689743)
- Profile response with avatar url (98017dfed3d8f06b0d2df5f005a48096d54577f4)
- Add AuthenticationID 🔑 (95b70cc36741422e6b97d49bbfb74e3a2fa88b13)
- **backend-181:** Send data to ElasticSearch (112a4b8405f90e498b4a22b9f340ded0da0c23b8)
- Support for multiple profiles (64858c924db72009c76e63cc9e7f0316b7c6ea18)
- **backend-153:** Followers, IsFollowing and Friends methods (4e4bcbf4636bf6873a5bbbe566702a4c2e331808)
- **backend-211:** Update service to use shared lib (c732e76a06b26d9dc32bffe1ad1e0fcf0842af04)
- **backend-222:** Inject service configs through Helm (0fb70d56d034cf4582e350b0f3be7d8c089aa9ed)
- **backend-155:** Blocked (and Unfollow profile) method (2cd886fd727fa344ee474b07275000a7d6e64ef3)
- **backend-227:** Mexico endpoints for Follow and Blocked (c3c33b5ea81a516ae503160697e0660fb7217818)
- **backend-262:** Counters for followers and following (aadfd2cec0cd79c2bc89d9600ca9bfd5a823b0df)
- **backend-260:** Update data through Mexico (de1c83e376585195d64686f9a996b4b30521eab5)
- **backend-260:** Update data through Mexico (93acd428af171bb9261dd0d830407fac877bec65)
- **backend-258:** Verify blocked users, Marnoto (812d93bcc47197303edb7c9530adb56002a2e3eb)

### Fixes

- **run:** Temporarily added database credentials to database (06ac09628414971c370200f65dbb66930fe2072b)
- **pipelines:** Fixed build command for build stage (174f7589c481b47abceb713511b5c60f80eef897)
- detect environment from command line (0e0634a01592d154df6e19cce904745b48709869)
- Use .mysql database specifier to get connection (53cd056ea453ae4945540c71a19ad525e119fc75)
- Add mexico endpoint to chart 😅 (1dd8eea9f20b20e5efc77eca76660f6eb73e8fd8)
- Improve database connectable with cached connections and autorelease (a32375bd2bc560ad59f7bf210c5fb2ee309f9536)
- Use cached connections (e34edcc4e3dab5639f9291f06bc4531cd83e6430)
- Add firebaseID to profile (df5d773c56b3b9f7669e04f78a5a687666f505d5)
- ElasticSearch client connection (78586edeee9441ee12c0961f527dbf0371c29f7d)
- Use connection pool with release closure instead of cached connection (e0e13c5d69d32d10f6d98d8b8b16dc6841b50fd0)
- Validate if photoURL is not empty (2d4b58596e6f75e1c87dd308114115c55927bc20)
- Add profiles path to chart on ingress (2eaaa59d77632e9735d2ccb5ff8526151cbb1b2f)
- Add user to teatro url for Profile response (b54725608ca67036bfce45737617808431cfe508)
- Add image for teatro endpoint (1ff927599a4b80584a87d4b514d79214820e8c1f)
- Return notFound when Profile not found (53341685b89c3d8ebbb41faa9e64f9f806648c64)
- **notrack:** Change `total` to `count` (c1a4d01e07347c4add1aecc6b65e18e8ca31e759)
- **notrack:** Change `total` to `count` (e13cf087f460d7d462dff9b5bfb6f851499a8422)