import Vapor
import YouClap

final class MexicoHTTPController: RouterController {
    private let service: MexicoServiceRepresentable

    init(service: MexicoServiceRepresentable) {
        self.service = service
    }

    func boot(router: Router) throws {
        let marnotoMexico = router.grouped("marnoto/mexico/user")

        marnotoMexico.post(MexicoUser.self, use: createOrUpdate)
        marnotoMexico.patch(MexicoUser.self, use: updatedUserAndProfile)
        marnotoMexico.delete(String.parameter, use: delete)

        marnotoMexico.post(MexicoFollower.self, at: "follower", use: createFollower)
        marnotoMexico.delete(String.parameter, "follower", String.parameter, use: unfollow)

        marnotoMexico.post(MexicoBlockedProfile.self, at: "blocked", use: createBlockedProfile)
        marnotoMexico.delete(String.parameter, "blocked", String.parameter, use: unblock)
    }

    private func createOrUpdate(_ request: Request, mexicoUser: MexicoUser) throws -> Future<HTTPStatus> {
        return try service.create(mexicoUser)
            .map { _ in HTTPStatus.created }
    }

    private func delete(_ request: Request) throws -> Future<HTTPStatus> {
        let firebaseIdentifier = try self.firebaseIdentifier(from: request)

        return service.delete(firebaseIdentifier: firebaseIdentifier)
            .transform(to: .ok)
    }

    private func createFollower(_ request: Request, mexicoFollower: MexicoFollower) throws -> Future<HTTPStatus> {
        return service.createFollower(mexicoFollower)
            .map { _ in HTTPStatus.created }
    }

    private func unfollow(_ request: Request) throws -> Future<HTTPStatus> {
        guard let userID = try? request.parameters.next(String.self) else {
            throw Abort(.badRequest)
        }

        guard let followerID = try? request.parameters.next(String.self) else {
            throw Abort(.badRequest)
        }

        return service.user(userID, unfollow: followerID).map { $0 != nil ? HTTPStatus.ok : HTTPStatus.notFound }
    }

    private func createBlockedProfile(_ request: Request, mexicoBlockedProfile: MexicoBlockedProfile) throws
    -> Future<HTTPStatus> {
        return service.createBlockedProfile(mexicoBlockedProfile)
            .map { _ in HTTPStatus.created }
    }

    private func unblock(_ request: Request) throws -> Future<HTTPStatus> {
        guard let userID = try? request.parameters.next(String.self) else {
            throw Abort(.badRequest)
        }

        guard let blockedID = try? request.parameters.next(String.self) else {
            throw Abort(.badRequest)
        }

        return service.user(userID, unblock: blockedID).map { $0 != nil ? HTTPStatus.ok : HTTPStatus.notFound }
    }

    private func updatedUserAndProfile(_ request: Request, _ mexicoUser: MexicoUser) throws -> Future<HTTPStatus> {
        return service.update(mexicoUser).map { _ in HTTPStatus.noContent }
    }
}

private extension MexicoHTTPController {
    private func firebaseIdentifier(from request: Request) throws -> String {
        guard let firebaseIdentifier = try? request.parameters.next(String.self) else {
            throw Abort(.badRequest)
        }
        return firebaseIdentifier
    }
}

extension MexicoServiceError: AbortError {
    var status: HTTPResponseStatus {
        switch self {
        case .store: return .internalServerError
        case .userNotFound: return .notFound
        case .elasticsearch: return .created
        case .invalidEmail: return .badRequest
        }
    }

    var identifier: String {
        switch self {
        case .store: return "STORE"
        case .userNotFound: return "NOT_FOUND"
        case .elasticsearch: return "ELASTICSEARCH"
        case .invalidEmail: return "INVALID_EMAIL"
        }
    }

    var reason: String {
        switch self {
        case .store(let error): return error.localizedDescription
        case .userNotFound(let identifier): return "profile with firebase id \(identifier) not found"
        case .elasticsearch(let error): return error.localizedDescription
        case .invalidEmail(let error): return "E-mail is not valid. Error: \(error)"
        }
    }
}
