import Vapor
import YouClap

final class ProfileHTTPController: RouterController {
    private let service: ProfileServiceRepresentable

    init(service: ProfileServiceRepresentable) {
        self.service = service
    }

    func boot(router: Router) throws {
        let profile = router.grouped("profile")

        profile.post(Profile.Create.self, use: create)
        profile.get(Profile.ID.parameter, use: readForID)
        profile.get("/", use: read)
        profile.patch(Profile.UpdateContainer.self, at: Profile.ID.parameter, use: update)
        profile.delete("/", Profile.ID.parameter, use: delete)

        router.get("profiles", use: profiles)

        profile.get("/", Profile.ID.parameter, "followers", use: followers)
        profile.get("/", Profile.ID.parameter, "following", use: following)
        profile.post("/", Profile.ID.parameter, "follow", Profile.ID.parameter, use: follow)
        profile.delete("/", Profile.ID.parameter, "follow", Profile.ID.parameter, use: unfollow)
        profile.get("/", Profile.ID.parameter, "following", Profile.ID.parameter, use: isFollowing)
        profile.get("/", Profile.ID.parameter, "following", "count", use: followingTotal)
        profile.get("/", Profile.ID.parameter, "followers", "count", use: followersTotal)

        profile.get("/", Profile.ID.parameter, "friends", use: friends)

        profile.get("/", Profile.ID.parameter, "blocked", use: blocked)
        profile.post("/", Profile.ID.parameter, "block", Profile.ID.parameter, use: block)
        profile.delete("/", Profile.ID.parameter, "block", Profile.ID.parameter, use: unblock)
    }

    // MARK: - CRUD Methods

    private func create(_ request: Request, profile: Profile.Create) throws -> Future<Response> {
        let authenticationID = try request.authenticationID()

        return service.create(profile: profile, withAuthenticationID: authenticationID)
            .encode(status: .created, for: request)
    }

    private func readForID(_ request: Request) throws -> Future<Profile.Response> {
        _ = try request.authenticationID()

        let authenticatedProfileID = try? request.profileID()
        let profileID = try self.profileID(from: request)

        return service.profile(for: profileID, authenticatedProfileID: authenticatedProfileID)
    }

    private func read(_ request: Request) throws -> Future<Profile.Response> {
        let authenticationID = try request.authenticationID()

        if let username: String = try request.query.get(at: "username") {
            let authenticatedProfileID = try? request.profileID()

            return service.profile(forUsername: username, authenticatedProfileID: authenticatedProfileID)
        }

        return service.profile(forAuthenticationID: authenticationID)
    }

    private func update(_ request: Request, _ profileUpdates: Profile.UpdateContainer) throws
    -> Future<Profile.Response> {
        let authenticationID = try request.authenticationID()
        let profileID = try self.profileID(from: request)

        return service.update(profileID: profileID,
                              withAuthenticationID: authenticationID,
                              profileUpdates: profileUpdates)
    }

    private func delete(_ request: Request) throws -> Future<HTTPStatus> {
        let authenticationID = try request.authenticationID()
        let profileID = try self.profileID(from: request)

        return service.delete(profile: profileID, withAuthenticationID: authenticationID)
            .transform(to: .ok)
    }

    private func profiles(_ request: Request) throws -> Future<[Profile.Response]> {
        guard let ids = try? request.query.get([Profile.ID].self, at: "id") else {
            throw Abort(.badRequest)
        }

        let authenticatedProfileID = try? request.profileID()

        return service.profiles(for: ids, authenticatedProfileID: authenticatedProfileID)
    }

    // MARK: - Following Methods

    private func followers(_ request: Request) throws -> Future<[Profile.Response]> {
        _ = try request.authenticationID()

        let requestProfileID = try request.profileID()
        let profileID = try self.profileID(from: request)

        return service.followers(for: profileID, andFor: requestProfileID)
    }

    private func following(_ request: Request) throws -> Future<[Profile.Response]> {
        _ = try request.authenticationID()

        let requestProfileID = try request.profileID()
        let profileID = try self.profileID(from: request)

        return service.following(for: profileID, andFor: requestProfileID)
    }

    private func follow(_ request: Request) throws -> Future<HTTPStatus> {
        _ = try request.authenticationID()

        let authenticatedUserID = try request.userID()

        let fromProfileID = try self.profileID(from: request)
        let toProfileID = try self.profileID(from: request)

        let followerCreate = Follower.Create(followerUserID: authenticatedUserID,
                                             followerProfileID: fromProfileID,
                                             followingProfileID: toProfileID)

        return service.startFollowing(follower: followerCreate)
            .transform(to: .created)
    }

    private func isFollowing(_ request: Request) throws -> Future<Profile.IsFollowing> {
        _ = try request.authenticationID()

        let fromProfileID = try self.profileID(from: request)
        let toProfileID = try self.profileID(from: request)

        return service.isFollowing(from: fromProfileID, to: toProfileID)
    }

    private func followingTotal(_ request: Request) throws -> Future<Profile.FollowingTotal> {
        _ = try request.authenticationID()

        let profileID = try self.profileID(from: request)

        return service.followingTotal(for: profileID)
    }

    private func followersTotal(_ request: Request) throws -> Future<Profile.FollowersTotal> {
        _ = try request.authenticationID()

        let profileID = try self.profileID(from: request)

        return service.followersTotal(for: profileID)
    }

    private func friends(_ request: Request) throws -> Future<[Profile.Response]> {
        _ = try request.authenticationID()

        let profileID = try self.profileID(from: request)

        return service.friends(for: profileID)
    }

    private func unfollow(_ request: Request) throws -> Future<HTTPStatus> {
        _ = try request.authenticationID()

        let authenticatedUserID = try request.userID()

        let fromProfileID = try self.profileID(from: request)
        let toProfileID = try self.profileID(from: request)

        let followerCreate = Follower.Create(followerUserID: authenticatedUserID,
                                             followerProfileID: fromProfileID,
                                             followingProfileID: toProfileID)

        return service.unfollowProfile(follower: followerCreate)
            .transform(to: .noContent)
    }

    // MARK: - Blocked Profile Methods

    private func blocked(_ request: Request) throws -> Future<[Profile.Response]> {
        _ = try request.authenticationID()

        let profileID = try self.profileID(from: request)

        return service.blocked(for: profileID)
    }

    private func block(_ request: Request) throws -> Future<HTTPStatus> {
        _ = try request.authenticationID()

        let authenticatedUserID = try request.userID()

        let fromProfileID = try self.profileID(from: request)
        let toProfileID = try self.profileID(from: request)

        let blockedProfile = BlockedProfile.Create(userID: authenticatedUserID,
                                                   profileID: fromProfileID,
                                                   blockedProfileID: toProfileID)

        return service.blockProfile(blockedProfile: blockedProfile)
            .transform(to: .created)
    }

    private func unblock(_ request: Request) throws -> Future<HTTPStatus> {
        _ = try request.authenticationID()

        let authenticatedUserID = try request.userID()

        let fromProfileID = try self.profileID(from: request)
        let toProfileID = try self.profileID(from: request)

        let blockedProfile = BlockedProfile.Create(userID: authenticatedUserID,
                                                   profileID: fromProfileID,
                                                   blockedProfileID: toProfileID)

        return service.unblockProfile(blockedProfile: blockedProfile)
            .transform(to: .noContent)
    }

    // MARK: - Private Methods

    private func profileID(from request: Request) throws -> Profile.ID {
        guard let profileID = try? request.parameters.next(Profile.ID.self) else {
            throw Abort(.badRequest)
        }
        return profileID
    }
}

// MARK: - Debuggable

extension ProfileServiceError: AbortError {
    var status: HTTPResponseStatus {
        switch self {
        case .userNotFound, .missingProfile, .notFoundProfileWithUsername: return .notFound
        case .store: return .internalServerError
        case .userForbiddenToAccessProfile: return .forbidden
        case .avatarTypeDoesNotMatchSocialIdentifier: return .conflict
        case .missingType: return .conflict
        case .missingFirebaseIdentifierForProfile: return .conflict
        }
    }

    var identifier: String {
        switch self {
        case .missingProfile: return "MISSING_PROFILE"
        case .userNotFound: return "USER_NOT_FOUND"
        case .store: return "STORE"
        case .userForbiddenToAccessProfile: return "FORBIDDEN"
        case .notFoundProfileWithUsername: return "PROFILE_NOT_FOUND"
        case .avatarTypeDoesNotMatchSocialIdentifier: return "AVATAR_TYPE_DOES_NOT_MATCH_SOCIAL_IDENTIFIER"
        case .missingType: return "MISSING_TYPE"
        case .missingFirebaseIdentifierForProfile: return "MISSING FIREBASE IDENTIFIER"
        }
    }

    var reason: String {
        switch self {
        case .missingProfile(let profileID):
            return "profile \(profileID) not found"
        case .userNotFound(let authenticationID):
            return "user with authentication ID \(authenticationID) not found"
        case .store(let error):
            return error.localizedDescription
        case .userForbiddenToAccessProfile(let userID, let profileID):
            return "user \(userID) doesn't have access to profile \(profileID)"
        case .notFoundProfileWithUsername(let username):
            return "profile with \(username) not found"
        case .avatarTypeDoesNotMatchSocialIdentifier:
            return "Avatar Type does not match social identifier."
        case .missingType(let profileID):
            return "Missing type for \(profileID)"
        case .missingFirebaseIdentifierForProfile(let profileID):
            return "Missing Firebase Identifier for profile `\(profileID)`"
        }
    }

}
