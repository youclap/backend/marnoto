import Vapor
import YouClap

final class UserHTTPController: RouterController {
    private let service: UserServiceRepresentable

    init(service: UserServiceRepresentable) {
        self.service = service
    }

    // MARK: - RouterController Methods

    func boot(router: Router) throws {
        let users = router.grouped("user")

        users.post(User.Create.self, use: create)
        users.get(use: retrieveMe)
        users.get(User.ID.parameter, use: read)
        users.patch(User.UpdateContainer.self, use: update)
        users.delete(User.ID.parameter, use: delete)
    }

    // MARK: - CRUD Methods

    private func create(_ request: Request, user: User.Create) throws -> Future<Response> {
        return service.create(user: user)
            .encode(status: .created, for: request)
    }

    private func retrieveMe(_ request: Request) throws -> Future<User> {
        let authenticationID = try request.authenticationID()

        return service.user(forAuthenticationID: authenticationID)
    }

    private func read(_ request: Request) throws -> Future<User> {
        let userID = try self.userID(from: request)

        return service.user(with: userID)
    }

    private func update(_ request: Request, _ userUpdates: User.UpdateContainer) throws -> Future<User> {
        let authenticationID = try request.authenticationID()
        let userID = try request.userID()

        return service.update(with: userID, authenticationID: authenticationID, userUpdates: userUpdates)
    }

    private func delete(_ request: Request) throws -> Future<HTTPStatus> {
        let authenticationID = try request.authenticationID()
        let requestUserID = try self.userID(from: request)

        return service.delete(user: requestUserID, authenticationID: authenticationID)
            .transform(to: .ok)
    }

    // MARK: - Private Methods

    private func userID(from request: Request) throws -> User.ID {
        guard let userID = try? request.parameters.next(User.ID.self) else {
            throw Abort(.badRequest)
        }
        return userID
    }
}

// MARK: - Debuggable

// TODO: Improve error representation to provide more information
extension UserServiceError: AbortError {
    var status: HTTPResponseStatus {
        switch self {
        case .missingUser: return .notFound
        case .notFound: return .notFound
        case .store: return .internalServerError
        }
    }

    var identifier: String {
        switch self {
        case .missingUser: return "MISSING_USER"
        case .notFound: return "NOT_FOUND"
        case .store: return "STORE"
        }
    }

    var reason: String {
        switch self {
        case .missingUser(let userID):
            return "user \(userID) not found"
        case .notFound(let authIdentifier): return "user with auth identifier \(authIdentifier) not found"
        case .store(let error): return error.localizedDescription
        }
    }
}
