import Vapor
import YouClap

final class SignUpHTTPController: RouterController {
    private let service: SignUpServiceRepresentable

    init(service: SignUpServiceRepresentable) {
        self.service = service
    }

    // MARK: - RouterController Methods

    func boot(router: Router) throws {
        router.post(SignUp.self, at: "signup", use: signup)
    }

    private func signup(_ request: Request, signupData: SignUp) throws -> Future<Response> {
        let authenticationID = try request.authenticationID()

        return service.signup(signupData, authenticationID: authenticationID)
            .encode(status: .created, for: request)
    }
}

extension SignUpServiceError: AbortError {
    var status: HTTPResponseStatus {
        switch self {
        case .inconsistentFirebaseID, .invalidField: return .badRequest
        case .store: return .internalServerError
        }
    }

    var identifier: String {
        switch self {
        case .inconsistentFirebaseID, .invalidField: return "INCONSISTENT_DATA"
        case .store: return "STORE"
        }
    }

    var reason: String {
        switch self {
        case .inconsistentFirebaseID(let left, let right):
            return "Provided firebase ID \(right) is different from authentication \(left)"
        case .invalidField(.email(let email)):
            return "Invalid email: \(email)"
        case .invalidField(.username(let username)):
            return "Invalid username: \(username)"
        case .store(let error):
            return error.localizedDescription
        }
    }
}

extension AmericaServiceError: AbortError {
    var status: HTTPResponseStatus {
        switch self {
        case .store: return .internalServerError
        }
    }

    var identifier: String {
        switch self {
        case .store: return "AMERICA STORE"
        }
    }

    var reason: String {
        switch self {
        case .store(let error): return error.localizedDescription
        }
    }
}
