import struct MySQL.MySQLError
import Vapor

final class MexicoService<Store>: MexicoServiceRepresentable
where Store: MexicoStore & ElasticSearchStore & UserStore & ProfileStore {

    private let store: Store
    private let logger: Logger

    init(store: Store, logger: Logger) {
        self.store = store
        self.logger = logger
    }

    func create(_ mexicoUser: MexicoUser) -> Future<(User, Profile)> {
        let user = User(name: mexicoUser.displayName,
                        username: mexicoUser.username,
                        email: mexicoUser.email.isEmpty ? mexicoUser.username : mexicoUser.email,
                        createdDate: mexicoUser.timestamp)

        let (avatarType, facebookID) = self.avatarType(from: mexicoUser.photoURL)

        let profile = Profile(name: mexicoUser.displayName,
                              username: mexicoUser.username,
                              typeID: ProfileType.user.id!, //swiftlint:disable:this force_unwrapping
                              avatarID: avatarType.id!, //swiftlint:disable:this force_unwrapping
                              createdDate: mexicoUser.timestamp,
                              deleted: mexicoUser.deleted)

        return store.create(user: user,
                            profile: profile,
                            facebookID: mexicoUser.facebookID ?? facebookID,
                            firebaseIdentifier: mexicoUser.uid)
            .flatMap { [store] user, profile in try store.index(profile: profile).map { (user, profile) } }
            .mapError { error in
                self.logger.error("💥 failed to create user with error \(error)")
                switch error {
                case MexicoStoreError.notFound(let notFoundID): return MexicoServiceError.userNotFound(notFoundID)
                case MexicoStoreError.sql(let error): return MexicoServiceError.store(error)
                case ElasticSearchStoreError.generic(let error): return MexicoServiceError.elasticsearch(error)
                default: return MexicoServiceError.store(error)
                }
            }
    }

    func update(_ mexicoUser: MexicoUser) -> Future<(User, Profile)> {

        let updateUser = self.updateUser(from: mexicoUser)
        let updateProfileAndFacebookID = self.updateProfile(from: mexicoUser)

        return store.update(firebaseIdentifier: mexicoUser.uid,
                            facebookID: mexicoUser.facebookID,
                            updateUser: updateUser,
                            updateProfile: updateProfileAndFacebookID)
            .flatMap { [store] user, profile in try store.index(profile: profile).map { (user, profile) } }
            .mapError { error in
                self.logger.error("💥 failed to create user with error \(error)")
                switch error {
                case MexicoStoreError.notFound(let notFoundID): return MexicoServiceError.userNotFound(notFoundID)
                case MexicoStoreError.sql(let error): return MexicoServiceError.store(error)
                case ElasticSearchStoreError.generic(let error): return MexicoServiceError.elasticsearch(error)
                default: return MexicoServiceError.store(error)
                }
            }
    }

    func delete(firebaseIdentifier: String) -> Future<Void> {
        let user = store.user(withAuthenticationID: firebaseIdentifier)
            .map { user -> User in
                var user = user
                user.name = String.random(with: GlobalConstants.FieldSize.name)
                user.username = String.random(with: GlobalConstants.FieldSize.username)
                user.email = String.random(with: GlobalConstants.FieldSize.email)
                user.deleted = true
                return user
            }

        let profile = store.profile(withFirebaseID: firebaseIdentifier)
            .map { profile -> Profile in
                var profile = profile
                profile.name = String.random(with: GlobalConstants.FieldSize.name)
                profile.username = String.random(with: GlobalConstants.FieldSize.username)
                profile.avatarID = AvatarType.none.id! //swiftlint:disable:this force_unwrapping
                profile.deleted = true
                return profile
            }

        return user.flatMap { user in profile.map { (user, $0) } }
            .flatMap(store.delete)
            .flatMap { _, profile in
                let profileID = try profile.requireID()

                return try self.store.unindex(profileID: profileID).always {
                    self.logger.info("🎉 deleted profile \(profileID) elasticsearch index")
                }
            }
    }

    func createFollower(_ mexicoFollower: MexicoFollower) -> Future<Follower> {
        return store.createFollower(mexicoFollower: mexicoFollower)
    }

    func user(_ userID: String, unfollow followedID: String) -> Future<Follower?> {
        let userProfileID = store.profile(withFirebaseID: userID)

        let followedProfileID = store.profile(withFirebaseID: followedID)

        return userProfileID.flatMap { user in followedProfileID.map { (try user.requireID(), try $0.requireID()) } }
            .flatMap { [store] in store.user($0.0, unfollow: $0.1) }
    }

    func createBlockedProfile(_ mexicoBlockedProfile: MexicoBlockedProfile) -> Future<BlockedProfile> {
        return store.createBlockedProfile(mexicoBlockedProfile: mexicoBlockedProfile)
    }

    func user(_ userID: String, unblock blockedID: String) -> Future<BlockedProfile?> {
        let userProfileID = store.profile(withFirebaseID: userID)

        let blockedProfileID = store.profile(withFirebaseID: blockedID)

        return userProfileID.flatMap { user in blockedProfileID.map { (try user.requireID(), try $0.requireID()) } }
            .flatMap { [store] in store.user($0.0, unblock: $0.1) }
    }

    // MARK: - Private Methods

    private func avatarType(from photoURL: String?) -> (AvatarType, String?) {
        var facebookID: String?
        var avatarType = AvatarType.none

        switch photoURL {
        case let url? where url.contains("facebook"):
            if let fbID = self.facebookID(fromPhotoURL: url) {
                avatarType = .facebook
                facebookID = fbID
            }
        case let url? where url.isEmpty == false: avatarType = .local
        default: break
        }

        return (avatarType, facebookID)
    }

    private func facebookID(fromPhotoURL photoURL: String) -> String? {
        guard let range = photoURL.range(of: #"([0-9]+)"#, options: .regularExpression) else {
            return nil
        }

        return photoURL.substring(with: range)
    }

    private func updateUser(from mexicoUser: MexicoUser) -> (User) -> User {
        return {
            var user = $0
            user.name = mexicoUser.displayName
            user.username = mexicoUser.username
            if !mexicoUser.email.isEmpty {
                user.email = mexicoUser.email
            }
            user.createdDate = mexicoUser.timestamp
            user.deleted = mexicoUser.deleted

            return user
        }
    }

    private func updateProfile(from mexicoUser: MexicoUser) -> (Profile) throws -> (Profile, String?) {
        return {
            var profile = $0

            let (avatarType, facebookID) = self.avatarType(from: mexicoUser.photoURL)

            profile.name = mexicoUser.displayName
            profile.username = mexicoUser.username
            profile.avatarID = try avatarType.requireID()
            profile.createdDate = mexicoUser.timestamp
            profile.deleted = mexicoUser.deleted

            return (profile, facebookID)
        }
    }
}
