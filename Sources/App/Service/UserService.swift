import Foundation
import NIO
import YouClap

final class UserService<Store>: UserServiceRepresentable where Store: UserStore {
    private let store: Store

    init(store: Store) {
        self.store = store
    }

    func create(user: User.Create) -> EventLoopFuture<User> {
        let newUser = User(name: user.name,
                           username: user.username,
                           email: user.email)
        return store.create(newUser, withAuthenticationID: user.authenticationID)
            .mapError(UserServiceError.store)
    }

    func user(with userUID: User.ID) -> EventLoopFuture<User> {
        return store.read(by: userUID)
            .mapError {
                switch $0 {
                case CRUDStoreError.notFound: return UserServiceError.missingUser(userUID)
                default: return UserServiceError.store($0)
                }
            }
    }

    func user(forAuthenticationID authenticationID: String) -> EventLoopFuture<User> {
        return store.user(by: authenticationID)
            .mapError {
                switch $0 {
                case CRUDStoreError.notFound: return UserServiceError.notFound(authenticationID)
                default: return UserServiceError.store($0)
                }
            }
    }

    func update(with userID: User.ID, authenticationID: String, userUpdates: User.UpdateContainer)
    -> EventLoopFuture<User> {
        return store.user(by: authenticationID)
            .flatMap { [store] user in
                guard let id = user.id, userID == id else { throw CRUDStoreError.notFound }

                var user = user

                userUpdates.values.forEach {
                    switch $0 {
                    case .name(let name): user.name = name
                    }
                }

                return store.update(model: user)
            }
            .mapError {
                switch $0 {
                case CRUDStoreError.notFound: return UserServiceError.notFound(authenticationID)
                default: return UserServiceError.store($0)
                }
            }
    }

    func delete(user userID: User.ID, authenticationID: String) -> EventLoopFuture<User> {
        return store.user(by: authenticationID)
            .flatMap { [store] user in
                guard let id = user.id, userID == id else { throw CRUDStoreError.notFound }

                return store.delete(by: userID)
            }
            .mapError {
                switch $0 {
                case CRUDStoreError.notFound: return UserServiceError.notFound(authenticationID)
                default: return UserServiceError.store($0)
                }
            }
    }
}
