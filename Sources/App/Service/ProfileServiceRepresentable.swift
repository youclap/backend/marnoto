import protocol Vapor.Service
import NIO

enum ProfileServiceError: Error {
    case missingProfile(Profile.ID)
    case missingType(ProfileType.ID)
    case missingFirebaseIdentifierForProfile(Profile.ID)
    case notFoundProfileWithUsername(String)
    case userNotFound(String)
    case store(Error)
    case userForbiddenToAccessProfile(User.ID, Profile.ID)
    case avatarTypeDoesNotMatchSocialIdentifier(String)
}

protocol ProfileServiceRepresentable: Service {
    func create(profile: Profile.Create, withAuthenticationID authenticationID: String)
    -> EventLoopFuture<Profile.Response>
    func profile(for profileID: Profile.ID, authenticatedProfileID: Profile.ID?)
    -> EventLoopFuture<Profile.Response>
    func profile(forAuthenticationID authenticationID: String) -> EventLoopFuture<Profile.Response>
    func profile(forUsername username: String, authenticatedProfileID: Profile.ID?)
    -> EventLoopFuture<Profile.Response>
    func update(profileID: Profile.ID,
                withAuthenticationID authenticationID: String,
                profileUpdates: Profile.UpdateContainer) -> EventLoopFuture<Profile.Response>
    func delete(profile profileID: Profile.ID, withAuthenticationID authenticationID: String)
    -> EventLoopFuture<Void>

    func profiles(for ids: [Profile.ID], authenticatedProfileID: Profile.ID?) -> EventLoopFuture<[Profile.Response]>

    func followers(for id: Profile.ID, andFor requestProfileID: Profile.ID) -> EventLoopFuture<[Profile.Response]>
    func following(for id: Profile.ID, andFor requestProfileID: Profile.ID) -> EventLoopFuture<[Profile.Response]>
    func startFollowing(follower: Follower.Create) -> EventLoopFuture<Follower>
    func unfollowProfile(follower: Follower.Create) -> EventLoopFuture<Void>
    func isFollowing(from fromProfileID: Profile.ID, to toProfileID: Profile.ID) -> EventLoopFuture<Profile.IsFollowing>
    func followingTotal(for profileID: Profile.ID) -> EventLoopFuture<Profile.FollowingTotal>
    func followersTotal(for profileID: Profile.ID) -> EventLoopFuture<Profile.FollowersTotal>
    func friends(for id: Profile.ID) -> EventLoopFuture<[Profile.Response]>
    func blocked(for id: Profile.ID) -> EventLoopFuture<[Profile.Response]>
    func blockProfile(blockedProfile: BlockedProfile.Create) -> EventLoopFuture<BlockedProfile>
    func unblockProfile(blockedProfile: BlockedProfile.Create) -> EventLoopFuture<Void>
}
