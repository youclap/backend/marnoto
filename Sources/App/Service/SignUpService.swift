import Foundation
import YouClap

private enum Constants {
    enum RegularExpression {
        static let email = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        static let username = "^[a-zA-Z]+(?:[._-]?[a-zA-Z0-9])*$"
    }
}

final class SignUpService<Store>: SignUpServiceRepresentable
where Store: SignUpStore & ProfileStore & PorticoStore & ElasticSearchStore {
    private let store: Store
    private let americaService: AmericaServiceRepresentable
    private let eventLoop: EventLoop
    private let logger: Logger

    init(store: Store, americaService: AmericaServiceRepresentable, eventLoop: EventLoop, logger: Logger) {
        self.store = store
        self.americaService = americaService
        self.eventLoop = eventLoop
        self.logger = logger
    }

    // swiftlint:disable:next function_body_length
    func signup(_ signupData: SignUp, authenticationID: String) -> EventLoopFuture<Profile.Response> {
        guard authenticationID == signupData.firebaseID else {
            return eventLoop.newFailedFuture(
                error: SignUpServiceError.inconsistentFirebaseID(authenticationID, signupData.firebaseID))
        }

        let sanitizedSignUpData = SignUp(name: signupData.name,
                                         username: signupData.username.trim(),
                                         email: signupData.email.trim(),
                                         firebaseID: signupData.firebaseID,
                                         type: signupData.type,
                                         avatar: signupData.avatar,
                                         facebookID: signupData.facebookID)

        guard sanitizedSignUpData.username.matches(Constants.RegularExpression.username) else {
            return eventLoop.newFailedFuture(
                error: SignUpServiceError.invalidField(.username(sanitizedSignUpData.username)))
        }

        guard sanitizedSignUpData.email.matches(Constants.RegularExpression.email) else {
            return eventLoop.newFailedFuture(
                error: SignUpServiceError.invalidField(.email(sanitizedSignUpData.email)))
        }

        let user = User(name: sanitizedSignUpData.name,
                        username: sanitizedSignUpData.username,
                        email: sanitizedSignUpData.email)

        let createProfile: () throws -> Profile = {
            Profile(name: sanitizedSignUpData.name,
                    username: sanitizedSignUpData.username,
                    typeID: try sanitizedSignUpData.type.requireID(),
                    avatarID: try sanitizedSignUpData.avatar.requireID())
        }

        return store.signup(user: user,
                            createProfile: createProfile,
                            facebookID: sanitizedSignUpData.facebookID,
                            firebaseIdentifier: sanitizedSignUpData.firebaseID)
            .mapError(SignUpServiceError.store)
            .flatMap { user, profile -> EventLoopFuture<(User, Profile)> in
                self.americaService
                    .createdProfile(profile,
                                    user: user,
                                    facebookID: signupData.facebookID,
                                    firebaseIdentifier: authenticationID)
                    .map { _ in
                        self.logger.info("🎉 Sent message to create profile \(profile) in America 🇺🇸")
                        return (user, profile)
                    }
            }
            .flatMap { [firebaseID = sanitizedSignUpData.firebaseID] user, profile -> Future<Profile> in
                let userID = try user.requireID()
                let profileID = try profile.requireID()

                return self.store.updateClaims(firebaseID: firebaseID, userID: userID, profileID: profileID)
                    .map { _ in profile }
            }
            .flatMap { profile in
                try self.store.index(profile: profile).map { _ in
                    self.logger.info("🎉 created profile \(profile) on elasticsearch index")
                    return profile
                }
            }
            .flatMap { [store] profile in
                store.firebaseIdentifier(for: profile).and(store.socialIdentifier(for: profile)).map {
                    try profile.response(for: sanitizedSignUpData.avatar,
                                         profileType: sanitizedSignUpData.type,
                                         firebaseIdentifier: sanitizedSignUpData.firebaseID,
                                         socialIdentifier: $0.1)

                }
            }
    }
}
