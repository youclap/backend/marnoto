import protocol Vapor.Service
import NIO

enum UserServiceError: Error {
    case missingUser(User.ID)
    case notFound(String)
    case store(Swift.Error)
}

protocol UserServiceRepresentable: Service {
    func create(user: User.Create) -> EventLoopFuture<User>
    func user(with userID: User.ID) -> EventLoopFuture<User>
    func user(forAuthenticationID authenticationID: String) -> EventLoopFuture<User>
    func update(with userID: User.ID, authenticationID: String, userUpdates: User.UpdateContainer)
    -> EventLoopFuture<User>
    func delete(user userID: User.ID, authenticationID: String) -> EventLoopFuture<User>
}
