import Vapor
import YouClap

extension ProfileService {

    func followers(for id: Profile.ID, andFor requestProfileID: Profile.ID) -> EventLoopFuture<[Profile.Response]> {

        return store.followers(for: id, andFor: requestProfileID)
            .mapError { error -> ProfileServiceError in
                switch error {
                case ProfileStoreError.unknownProfileID(let profileID):
                    return ProfileServiceError.missingProfile(profileID)
                default: return ProfileServiceError.store(error)
                }
            }
            .map {
                try $0.map { responseFromModel in

                    guard let firebaseIdentifier = responseFromModel.firebaseIdentifier else {
                        throw ProfileServiceError.missingFirebaseIdentifierForProfile(responseFromModel.id)
                    }

                    let profileSocialIdentifier = ProfileSocialIdentifier(
                        profileID: responseFromModel.id,
                        socialNetworkTypeID: responseFromModel.socialNetworkTypeID,
                        identifier: responseFromModel.socialNetworkIdentifier)

                    return try Profile.Response(
                        id: responseFromModel.id,
                        name: responseFromModel.name,
                        username: responseFromModel.username,
                        type: responseFromModel.typeName,
                        createdDate: responseFromModel.createdDate,
                        deleted: responseFromModel.deleted,
                        avatarType: AvatarType(id: responseFromModel.avatarID),
                        firebaseIdentifier: firebaseIdentifier,
                        socialIdentifier: profileSocialIdentifier
                    )
                }
            }

    }

    func following(for id: Profile.ID, andFor requestProfileID: Profile.ID) -> EventLoopFuture<[Profile.Response]> {
        return store.following(for: id, andFor: requestProfileID)
            .mapError { error -> ProfileServiceError in
                switch error {
                case ProfileStoreError.unknownProfileID(let profileID):
                    return ProfileServiceError.missingProfile(profileID)
                default: return ProfileServiceError.store(error)
                }
            }
            .map {
                try $0.compactMap { responseFromModel in

                    guard let firebaseIdentifier = responseFromModel.firebaseIdentifier else {
                        throw ProfileServiceError.missingFirebaseIdentifierForProfile(responseFromModel.id)
                    }

                    let profileSocialIdentifier = ProfileSocialIdentifier(
                        profileID: responseFromModel.id,
                        socialNetworkTypeID: responseFromModel.socialNetworkTypeID,
                        identifier: responseFromModel.socialNetworkIdentifier)

                    return try Profile.Response(
                        id: responseFromModel.id,
                        name: responseFromModel.name,
                        username: responseFromModel.username,
                        type: responseFromModel.typeName,
                        createdDate: responseFromModel.createdDate,
                        deleted: responseFromModel.deleted,
                        avatarType: AvatarType(id: responseFromModel.avatarID),
                        firebaseIdentifier: firebaseIdentifier,
                        socialIdentifier: profileSocialIdentifier
                    )

                }
            }

    }

    func startFollowing(follower: Follower.Create) -> EventLoopFuture<Follower> {
        let follower = Follower(followerUserID: follower.followerUserID,
                                followerProfileID: follower.followerProfileID,
                                followingProfileID: follower.followingProfileID)

        return store.startFollowing(follower: follower)
            .flatMap { follower in
                self.americaService.followedProfile(follower).map { _ in
                    self.logger.info("🎉 Sent message to create follower \(follower) in America 🇺🇸")
                    return follower
                }
            }
            .mapError { error -> ProfileServiceError in
                switch error {
                case ProfileStoreError.unknownProfileID(let profileID):
                    return ProfileServiceError.missingProfile(profileID)
                default: return ProfileServiceError.store(error)
                }
            }
    }

    func unfollowProfile(follower: Follower.Create) -> EventLoopFuture<Void> {
        let follower = Follower(followerUserID: follower.followerUserID,
                                followerProfileID: follower.followerProfileID,
                                followingProfileID: follower.followingProfileID)

        return store.unfollow(follower: follower)
            .flatMap { _ in
                self.americaService.unfollowedProfile(follower).always {
                    self.logger.info("🎉 Sent message to unfollow \(follower) in America 🇺🇸")
                }
            }
            .mapError { error -> ProfileServiceError in
                switch error {
                case ProfileStoreError.unknownProfileID(let profileID):
                    return ProfileServiceError.missingProfile(profileID)
                default: return ProfileServiceError.store(error)
                }
            }
    }

    func isFollowing(from fromProfileID: Profile.ID, to toProfileID: Profile.ID)
                    -> EventLoopFuture<Profile.IsFollowing> {
        return store.isFollowing(from: fromProfileID, to: toProfileID)
            .mapError { error -> ProfileServiceError in
                switch error {
                case ProfileStoreError.unknownProfileID(let profileID):
                    return ProfileServiceError.missingProfile(profileID)
                default: return ProfileServiceError.store(error)
                }
            }
            .map { .init(isFollowing: $0) }
    }

    func followingTotal(for profileID: Profile.ID)
    -> EventLoopFuture<Profile.FollowingTotal> {
        return store.followingTotal(profileID: profileID)
            .mapError { error -> ProfileServiceError in
                switch error {
                case ProfileStoreError.unknownProfileID(let profileID):
                    return ProfileServiceError.missingProfile(profileID)
                default: return ProfileServiceError.store(error)
                }
            }
            .map(Profile.FollowingTotal.init)
    }

    func followersTotal(for profileID: Profile.ID)
    -> EventLoopFuture<Profile.FollowersTotal> {
        return store.followersTotal(profileID: profileID)
            .mapError { error -> ProfileServiceError in
                switch error {
                case ProfileStoreError.unknownProfileID(let profileID):
                    return ProfileServiceError.missingProfile(profileID)
                default: return ProfileServiceError.store(error)
                }
            }
            .map(Profile.FollowersTotal.init)
    }

    func friends(for id: Profile.ID) -> EventLoopFuture<[Profile.Response]> {
        return store.friends(for: id)
            .mapError { error -> ProfileServiceError in
                switch error {
                case ProfileStoreError.unknownProfileID(let profileID):
                    return ProfileServiceError.missingProfile(profileID)
                default: return ProfileServiceError.store(error)
                }
            }
            .map {
                try $0.compactMap { responseFromModel in

                    guard let firebaseIdentifier = responseFromModel.firebaseIdentifier else {
                        throw ProfileServiceError.missingFirebaseIdentifierForProfile(responseFromModel.id)
                    }

                    let profileSocialIdentifier = ProfileSocialIdentifier(
                        profileID: responseFromModel.id,
                        socialNetworkTypeID: responseFromModel.socialNetworkTypeID,
                        identifier: responseFromModel.socialNetworkIdentifier)

                    return try Profile.Response(
                        id: responseFromModel.id,
                        name: responseFromModel.name,
                        username: responseFromModel.username,
                        type: responseFromModel.typeName,
                        createdDate: responseFromModel.createdDate,
                        deleted: responseFromModel.deleted,
                        avatarType: AvatarType(id: responseFromModel.avatarID),
                        firebaseIdentifier: firebaseIdentifier,
                        socialIdentifier: profileSocialIdentifier
                    )

                }
            }
    }
}
