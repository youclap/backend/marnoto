import Vapor

enum MexicoServiceError: Error {
    case store(Error)
    case userNotFound(String)
    case elasticsearch(Error)
    case invalidEmail(String)
}

protocol MexicoServiceRepresentable: Service {
    func create(_ mexicoUser: MexicoUser) throws -> Future<(User, Profile)>
    func update(_ mexicoUser: MexicoUser) -> Future<(User, Profile)>
    func delete(firebaseIdentifier: String) -> Future<Void>

    func createFollower(_ mexicoFollower: MexicoFollower) -> Future<Follower>
    func user(_ userID: String, unfollow followedID: String) -> Future<Follower?>

    func createBlockedProfile(_ mexicoBlockedProfile: MexicoBlockedProfile) -> Future<BlockedProfile>
    func user(_ userID: String, unblock blockedID: String) -> Future<BlockedProfile?>
}
