import NIO
import protocol Vapor.Service

enum AmericaServiceError: Error {
    case store(Error)
}

protocol AmericaServiceRepresentable: Service {
    func createdProfile(_ profile: Profile, user: User, facebookID: String?, firebaseIdentifier: String)
    -> EventLoopFuture<Void>
    func updatedProfile(_ profile: Profile, firebaseIdentifier: String) -> EventLoopFuture<Void>
    func markDeletedProfile(_ firebaseIdentifier: String) -> EventLoopFuture<Void>
    func deletedProfile(_ firebaseIdentifier: String) -> EventLoopFuture<Void>

    func blockedProfile(_ blockedProfile: BlockedProfile) -> EventLoopFuture<Void>
    func unblockedProfile(_ blockedProfile: BlockedProfile) -> EventLoopFuture<Void>

    func followedProfile(_ follower: Follower) -> EventLoopFuture<Void>
    func unfollowedProfile(_ follower: Follower) -> EventLoopFuture<Void>
}
