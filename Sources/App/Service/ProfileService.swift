import NIO
import YouClap

final class ProfileService<Store>: ProfileServiceRepresentable
where Store: ProfileStore & UserStore & ElasticSearchStore {
    let store: Store
    let americaService: AmericaServiceRepresentable
    let logger: Logger

    init(store: Store, americaService: AmericaServiceRepresentable, logger: Logger) {
        self.store = store
        self.americaService = americaService
        self.logger = logger
    }

    func create(profile: Profile.Create, withAuthenticationID authenticationID: String)
    -> EventLoopFuture<Profile.Response> {
        return store.user(by: authenticationID)
            .mapError { error -> ProfileServiceError in
                switch error {
                case CRUDStoreError.notFound: return ProfileServiceError.userNotFound(authenticationID)
                default: return ProfileServiceError.store(error)
                }
            }
            .flatMap { [store] user -> Future<(Profile, AvatarType, ProfileType)> in
                let newProfile = Profile(name: profile.name,
                                         username: profile.username,
                                         typeID: try profile.type.requireID(),
                                         avatarID: try profile.avatar.requireID())

                return store.create(profile: newProfile, for: user, firebaseIdentifier: authenticationID)
                    .mapError(ProfileServiceError.store)
                    .map { ($0, profile.avatar, profile.type) }
            }
            .flatMap { [store] profile, avatarType, profileType -> Future<(Profile, AvatarType, ProfileType)> in
                try store.index(profile: profile).map { _ in (profile, avatarType, profileType) }
            }
            .flatMap { [store] profile, avatarType, profileType in
                store.firebaseIdentifier(for: profile).and(store.socialIdentifier(for: profile)).map {
                    try profile.response(for: avatarType,
                                         profileType: profileType,
                                         firebaseIdentifier: $0.0.firebaseID,
                                         socialIdentifier: $0.1)
                }
            }
    }

    func profile(for profileID: Profile.ID, authenticatedProfileID: Profile.ID?)
    -> EventLoopFuture<Profile.Response> {
        return { authenticatedProfileID -> Future<Profile> in
            guard let authenticatedProfileID = authenticatedProfileID else {
                return store.profile(for: profileID)
            }

            return store.profile(for: profileID, authenticatedID: authenticatedProfileID)
        }(authenticatedProfileID)
            .mapError { error -> ProfileServiceError in
                switch error {
                case CRUDStoreError.notFound:
                    return ProfileServiceError.missingProfile(profileID)
                case ProfileStoreError.unknownProfileID(let profileID):
                    return ProfileServiceError.missingProfile(profileID)
                default:
                    return ProfileServiceError.store(error)
                }
            }
            .buildResponse(store)
    }

    func profile(forAuthenticationID authenticationID: String) -> EventLoopFuture<Profile.Response> {
        return store.user(by: authenticationID)
            .flatMap { [store] in store.profile(for: $0) }
            .mapError {
                switch $0 {
                case CRUDStoreError.notFound:
                    return ProfileServiceError.userNotFound(authenticationID)
                case ProfileStoreError.notFoundProfileForUser:
                    return ProfileServiceError.userNotFound(authenticationID)
                case ProfileStoreError.unknownProfileID(let profileID):
                    return ProfileServiceError.missingProfile(profileID)
                default: return ProfileServiceError.store($0)
                }
            }
            .buildResponse(store)
    }

    func profile(forUsername username: String, authenticatedProfileID: Profile.ID?)
    -> EventLoopFuture<Profile.Response> {
        return { authenticatedProfileID -> Future<Profile> in
            guard let authenticatedProfileID = authenticatedProfileID else {
                return store.profile(for: username)
            }

            return store.profile(for: username, authenticatedID: authenticatedProfileID)
        }(authenticatedProfileID)
            .mapError { error in
                switch error {
                case CRUDStoreError.notFound:
                    return ProfileServiceError.notFoundProfileWithUsername(username)
                case ProfileStoreError.notFoundProfileForUser:
                    return ProfileServiceError.notFoundProfileWithUsername(username)
                case ProfileStoreError.unknownProfileID(let profileID):
                    return ProfileServiceError.missingProfile(profileID)
                case ProfileStoreError.unknownProfileWithUsername(let username):
                    return ProfileServiceError.notFoundProfileWithUsername(username)
                default: return ProfileServiceError.store(error)
                }
            }
            .buildResponse(store)
    }

    func update(profileID: Profile.ID,
                withAuthenticationID authenticationID: String,
                profileUpdates: Profile.UpdateContainer)
    -> EventLoopFuture<Profile.Response> {
        return userHasAccessToProfile(authenticationID: authenticationID, profileID: profileID)
            .flatMap { profile, _ in
                var profile = profile

                guard profile.deleted == false else {
                    throw ProfileStoreError.unknownProfileID(profileID)
                }

                try profileUpdates.values.forEach {
                    switch $0 {
                    case .name(let name): profile.name = name
                    case .avatar(let avatar): profile.avatarID = try avatar.requireID()
                    }
                }

                let updateUser: (User) -> (User) = { user in
                    var user = user

                    profileUpdates.values.forEach {
                        switch $0 {
                        case .name(let name): user.name = name
                        case .avatar: break
                        }
                    }

                    return user
                }

                return self.store.update(profile: profile, and: updateUser)
                    .mapError { error -> ProfileServiceError in
                        switch error {
                        case CRUDStoreError.notFound: return ProfileServiceError.missingProfile(profileID)
                        default: return ProfileServiceError.store(error)
                        }
                    }
                    .map { $0.0 }
            }
            .flatMap { [store] profile -> Future<Profile> in
                try store.index(profile: profile).map { _ in profile }
            }
            .flatMap { profile in
                self.americaService.updatedProfile(profile, firebaseIdentifier: authenticationID)
                    .map { _ in
                        self.logger.info("🎉 Sent message to update profile \(profile) in America 🇺🇸")
                        return profile
                    }
            }
            .buildResponse(store)
    }

    func delete(profile profileID: Profile.ID,
                withAuthenticationID authenticationID: String)
    -> EventLoopFuture<Void> {
        return userHasAccessToProfile(authenticationID: authenticationID, profileID: profileID)
            .flatMap { [store] oldProfile, oldUser in
                var user = oldUser
                user.name = String.random(with: GlobalConstants.FieldSize.name)
                user.username = String.random(with: GlobalConstants.FieldSize.username)
                user.email = String.random(with: GlobalConstants.FieldSize.email)
                user.deleted = true

                var profile = oldProfile
                profile.name = String.random(with: GlobalConstants.FieldSize.name)
                profile.username = String.random(with: GlobalConstants.FieldSize.username)
                profile.avatarID = AvatarType.none.id! //swiftlint:disable:this force_unwrapping
                profile.deleted = true

                return store.delete(profile: profile, user: user)
                    .mapError { error -> ProfileServiceError in
                        switch error {
                        case CRUDStoreError.notFound: return ProfileServiceError.missingProfile(profileID)
                        default: return ProfileServiceError.store(error)
                        }
                    }
                    .map { _ in
                        self.logger.info("🎉 Deleted profile \(profileID) and firebaseID \(authenticationID)")
                        return ()
                    }
            }
            .flatMap {
                try self.store.unindex(profileID: profileID).always {
                    self.logger.info("🎉 deleted profile \(profileID) from elasticsearch index")
                }
            }
            .flatMap {
                self.americaService.deletedProfile(authenticationID).always {
                    self.logger.info("🎉 Sent message to delete profile \(profileID) in America 🇺🇸")
                }
            }
    }

    func profiles(for ids: [Profile.ID], authenticatedProfileID: Profile.ID?) -> EventLoopFuture<[Profile.Response]> {
        return { authenticatedProfileID -> Future<[Profile]> in
            guard let authenticatedProfileID = authenticatedProfileID else {
                return store.profiles(for: ids)
            }

            return store.profiles(for: ids, authenticatedProfileID: authenticatedProfileID)
        }(authenticatedProfileID)
            .flatMap { [store] profiles in
                let profileInfoFutures = profiles
                    .filter { !$0.deleted }
                    .map { profile in profile.buildResponse(store) }

                return EventLoopFuture.whenAll(profileInfoFutures, eventLoop: store.databaseConnectable.eventLoop)
            }
            .mapError(ProfileServiceError.store)
    }

    // MARK: - Private Methods

    private func userHasAccessToProfile(authenticationID: String,
                                        profileID: Profile.ID) -> EventLoopFuture<(Profile, User)> {
        return store.profile(for: profileID).and(store.user(by: authenticationID))
            .mapError { error -> ProfileServiceError in
                switch error {
                case CRUDStoreError.notFound: return ProfileServiceError.missingProfile(profileID)
                default: return ProfileServiceError.store(error)
                }
            }
            .flatMap { [store] profile, user in
                store.user(user, canReadProfile: profile)
                    .mapError { error -> ProfileServiceError in
                        switch error {
                        case CRUDStoreError.notFound: return ProfileServiceError.missingProfile(profileID)
                        default: return ProfileServiceError.store(error)
                        }
                    }
                    .map { hasAccess in
                        guard hasAccess == true else {
                            // swiftlint:disable:next force_unwrapping
                            throw ProfileServiceError.userForbiddenToAccessProfile(user.id!, profileID)
                        }

                        return (profile, user)
                    }
            }
    }
}

extension Profile {
    func buildResponse(_ store: ProfileStore) -> Future<Profile.Response> {
        return store.avatarType(for: self)
            .and(store.profileType(for: self))
            .and(store.firebaseIdentifier(for: self))
            .and(store.socialIdentifier(for: self))
            .map { ($0.0.0.0, $0.0.0.1, $0.0.1.firebaseID, $0.1) }
            .map(response)
    }

    func response(for avatarType: AvatarType,
                  profileType: ProfileType,
                  firebaseIdentifier: String,
                  socialIdentifier: ProfileSocialIdentifier?) throws -> Profile.Response {
        switch (avatarType, socialIdentifier) {
        case (AvatarType.none, _):
            return try Profile.Response(profile: self, profileType: profileType)
        case (AvatarType.local, _):
            let avatarURL = Utils.teatroURL(withIdentifier: firebaseIdentifier)
            return try Profile.Response(profile: self, profileType: profileType, avatarURL: avatarURL)
        case (AvatarType.facebook, let socialIdentifier?):
            let avatarURL = Utils.facebookURL(withIdentifier: socialIdentifier.identifier)
            return try Profile.Response(profile: self, profileType: profileType, avatarURL: avatarURL)
        default:
            assertionFailure("""
Avatar Type and Social Identifier does not match!
(Maybe the error is Avatar Type is facebook, but does not exists Social Identifier in database!)
""")
            throw ProfileServiceError.avatarTypeDoesNotMatchSocialIdentifier("""
Avatar Type and Social Identifier does not match!
""")
        }
    }
}

extension EventLoopFuture where T == Profile {
    func buildResponse(_ store: ProfileStore) -> Future<Profile.Response> {
        return self.flatMap { profile in profile.buildResponse(store) }
    }
}
