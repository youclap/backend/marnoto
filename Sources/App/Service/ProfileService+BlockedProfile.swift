import Vapor
import YouClap

extension ProfileService {

    func blocked(for id: Profile.ID) -> EventLoopFuture<[Profile.Response]> {
        return store.blocked(for: id)
            .mapError { error -> ProfileServiceError in
                switch error {
                case ProfileStoreError.unknownProfileID(let profileID):
                    return ProfileServiceError.missingProfile(profileID)
                default: return ProfileServiceError.store(error)
                }
            }
        .map {
            try $0.map { responseFromModel in

                guard let firebaseIdentifier = responseFromModel.firebaseIdentifier else {
                    throw ProfileServiceError.missingFirebaseIdentifierForProfile(responseFromModel.id)
                }

                let profileSocialIdentifier = ProfileSocialIdentifier(
                    profileID: responseFromModel.id,
                    socialNetworkTypeID: responseFromModel.socialNetworkTypeID,
                    identifier: responseFromModel.socialNetworkIdentifier)

                return try Profile.Response(
                    id: responseFromModel.id,
                    name: responseFromModel.name,
                    username: responseFromModel.username,
                    type: responseFromModel.typeName,
                    createdDate: responseFromModel.createdDate,
                    deleted: responseFromModel.deleted,
                    avatarType: AvatarType(id: responseFromModel.avatarID),
                    firebaseIdentifier: firebaseIdentifier,
                    socialIdentifier: profileSocialIdentifier
                )
            }
        }
    }

    func blockProfile(blockedProfile: BlockedProfile.Create) -> EventLoopFuture<BlockedProfile> {
        let blockedProfile = BlockedProfile(userID: blockedProfile.userID,
                                            profileID: blockedProfile.profileID,
                                            blockedProfileID: blockedProfile.blockedProfileID)

        return store.blockProfile(blockedProfile: blockedProfile)
            .flatMap { blockedProfile in
                self.americaService.blockedProfile(blockedProfile).map { _ in
                    self.logger.info("🎉 Sent message to block profile \(blockedProfile) in America 🇺🇸")
                    return blockedProfile
                }
            }
            .mapError { error -> ProfileServiceError in
                switch error {
                case ProfileStoreError.unknownProfileID(let profileID):
                    return ProfileServiceError.missingProfile(profileID)
                default: return ProfileServiceError.store(error)
                }
            }

    }

    func unblockProfile(blockedProfile: BlockedProfile.Create) -> EventLoopFuture<Void> {
        let blockedProfile = BlockedProfile(userID: blockedProfile.userID,
                                            profileID: blockedProfile.profileID,
                                            blockedProfileID: blockedProfile.blockedProfileID)

        return store.unblockProfile(blockedProfile: blockedProfile)
            .flatMap { _ in
                self.americaService.unblockedProfile(blockedProfile).always {
                    self.logger.info("🎉 Sent message to delete blocked profile \(blockedProfile) in America 🇺🇸")
                }
            }
            .mapError { error -> ProfileServiceError in
                switch error {
                case ProfileStoreError.unknownProfileID(let profileID):
                    return ProfileServiceError.missingProfile(profileID)
                default: return ProfileServiceError.store(error)
                }
            }
    }

}
