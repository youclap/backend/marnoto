import GoogleCloudPubSubKit
import NIO
import YouClap

typealias AmericaUser = MexicoUser
typealias AmericaBlockedProfile = MexicoBlockedProfile
typealias AmericaFollower = MexicoFollower

final class AmericaService<Store>: AmericaServiceRepresentable where Store: AmericaStore & ProfileStore & UserStore {
    private let logger: Logger
    private let store: Store

    init(store: Store, logger: Logger) {
        self.store = store
        self.logger = logger
    }

    func createdProfile(_ profile: Profile, user: User, facebookID: String?, firebaseIdentifier: String)
    -> EventLoopFuture<Void> {

        // swiftlint:disable force_unwrapping
        let photoURL: String? = {
            switch (profile.avatarID, facebookID) {
            case (AvatarType.none.id!, _): return nil
            case (AvatarType.local.id!, _): return firebaseIdentifier
            case (AvatarType.facebook.id!, let facebookID?): return Utils.facebookURL(withIdentifier: facebookID)
            default: return nil
            }
        }()
        // swiftlint:enable force_unwrapping

        let americaUser = AmericaUser(displayName: profile.name,
                                      email: user.email,
                                      facebookID: facebookID,
                                      photoURL: photoURL,
                                      timestamp: profile.createdDate ?? Date(),
                                      uid: firebaseIdentifier,
                                      username: user.username)

        let documentInfo = AmericaDocumentInfo(model: americaUser, collection: .user, documentID: firebaseIdentifier)

        let americaUserUsername = AmericaUserUsername(email: user.email, userID: firebaseIdentifier)
        let americaUsernameInfo = AmericaDocumentInfo(model: americaUserUsername,
                                                      collection: .userUsername,
                                                      documentID: user.username)

        return store.create(documents: documentInfo, americaUsernameInfo)
            .mapError(AmericaServiceError.store)
            .map {
                self.logger.info("🎉 created message with id \($0.messageIds)")

                return ()
            }
    }

    func updatedProfile(_ profile: Profile, firebaseIdentifier: String) -> EventLoopFuture<Void> {

        let userForProfileFuture = store.user(by: firebaseIdentifier)
        let socialIdentifierFuture = store.socialIdentifier(for: profile)

        return userForProfileFuture.and(socialIdentifierFuture).flatMap { user, socialIdentifier in
            let (facebookID, photoURL): (String?, String?) = {
                // swiftlint:disable force_unwrapping
                switch (profile.avatarID, socialIdentifier) {
                case (AvatarType.none.id!, _):
                    return (nil, nil)
                case (AvatarType.local.id!, _):
                    return (nil, firebaseIdentifier)
                case (AvatarType.facebook.id!, let socialIdentifier?)
                where socialIdentifier.socialNetworkTypeID == SocialNetworkType.facebook.id!:
                    return (socialIdentifier.identifier, Utils.facebookURL(withIdentifier: socialIdentifier.identifier))
                default:
                    return (nil, nil)
                }
                // swiftlint:enable force_unwrapping
            }()

            let createdDate: Date = {
                guard let date = profile.createdDate else {
                    assertionFailure("💥 missing createdDate from an existing profile 👉 \(profile)")
                    return Date()
                }

                return date
            }()

            let document = AmericaUser(displayName: profile.name,
                                       email: user.email,
                                       facebookID: facebookID,
                                       photoURL: photoURL,
                                       timestamp: createdDate,
                                       uid: firebaseIdentifier,
                                       username: profile.username)

            let documentInfo = AmericaDocumentInfo(model: document, collection: .user, documentID: firebaseIdentifier)

            return self.store.update(documents: documentInfo)
                .mapError(AmericaServiceError.store)
                .map { response -> Void in
                    self.logger.info("🎉 created message with id \(response.messageIds)")

                    return ()
                }
        }
    }

    func markDeletedProfile(_ firebaseIdentifier: String) -> EventLoopFuture<Void> {
        return self.store.markDeleted(collection: .user, documentID: firebaseIdentifier)
            .mapError(AmericaServiceError.store)
            .map { response -> Void in
                self.logger.info("🎉 created message with id \(response.messageIds)")

                return ()
            }
    }

    func deletedProfile(_ firebaseIdentifier: String) -> EventLoopFuture<Void> {
        return self.store.delete(collection: .user, documentID: firebaseIdentifier)
            .mapError(AmericaServiceError.store)
            .map { response -> Void in
                self.logger.info("🎉 created message with id \(response.messageIds)")

                return ()
            }
    }

    // MARK: - Block/Unblock Profile

    func blockedProfile(_ blockedProfile: BlockedProfile) -> EventLoopFuture<Void> {
        return firebaseIdentifiers(for: blockedProfile.profileID, blockedProfile.blockedProfileID)
            .flatMap { profileFirebaseID, blockedProfileFirebaseID in
                let documentID = "\(profileFirebaseID)_\(blockedProfileFirebaseID)"

                let createdDate: Date = {
                    guard let date = blockedProfile.createdDate else {
                        assertionFailure("💥 missing createdDate from an existing blocked profile 👉 \(blockedProfile)")
                        return Date()
                    }

                    return date
                }()

                let document = AmericaBlockedProfile(blockedID: blockedProfileFirebaseID,
                                                     timestamp: createdDate,
                                                     userID: profileFirebaseID)

                let documentInfo = AmericaDocumentInfo(model: document,
                                                       collection: .blockedUser,
                                                       documentID: documentID)

                return self.store.create(documents: documentInfo)
                    .mapError(AmericaServiceError.store)
                    .map { response -> Void in
                        self.logger.info("🎉 created message with id \(response.messageIds)")

                        return ()
                    }
            }
    }

    func unblockedProfile(_ blockedProfile: BlockedProfile) -> EventLoopFuture<Void> {
        return firebaseIdentifiers(for: blockedProfile.profileID, blockedProfile.blockedProfileID)
            .flatMap { profileFirebaseID, blockedProfileFirebaseID in
                let documentID = "\(profileFirebaseID)_\(blockedProfileFirebaseID)"

                return self.store.delete(collection: .blockedUser, documentID: documentID)
                    .mapError(AmericaServiceError.store)
                    .map { response -> Void in
                        self.logger.info("🎉 created message with id \(response.messageIds)")

                        return ()
                    }
            }
    }

    // MARK: - Follow/Unfollow Profile

    func followedProfile(_ follower: Follower) -> EventLoopFuture<Void> {
        return firebaseIdentifiers(for: follower.followerProfileID, follower.followingProfileID)
            .flatMap { profileID, followedID in
                let documentID = "\(profileID)_\(followedID)"

                let createdDate: Date = {
                   guard let date = follower.createdDate else {
                       assertionFailure("💥 missing createdDate from an existing blocked profile 👉 \(follower)")
                       return Date()
                   }

                   return date
               }()

                let document = AmericaFollower(followedID: followedID, timestamp: createdDate, userID: profileID)

                let documentInfo = AmericaDocumentInfo(model: document, collection: .following, documentID: documentID)

                return self.store.create(documents: documentInfo)
                    .mapError(AmericaServiceError.store)
                    .map { response -> Void in
                        self.logger.info("🎉 created message with id \(response.messageIds)")

                        return ()
                    }
            }
    }

    func unfollowedProfile(_ follower: Follower) -> EventLoopFuture<Void> {
        return firebaseIdentifiers(for: follower.followerProfileID, follower.followingProfileID)
            .flatMap { profileID, followedID in
                let documentID = "\(profileID)_\(followedID)"

                return self.store.delete(collection: .following, documentID: documentID)
                    .mapError(AmericaServiceError.store)
                    .map { response -> Void in
                        self.logger.info("🎉 created message with id \(response.messageIds)")

                        return ()
                    }
            }
    }

    // MARK: - Private Methods

    private func firebaseIdentifiers(for firstProfile: Profile.ID, _ secondProfile: Profile.ID)
    -> EventLoopFuture<(String, String)> {
        let firstProfileFirebaseIDFuture = store.firebaseIdentifier(for: firstProfile)
        let secondProfileFirebaseIDFuture = store.firebaseIdentifier(for: secondProfile)

        return firstProfileFirebaseIDFuture.and(secondProfileFirebaseIDFuture)
            .map { ($0.0.firebaseID, $0.1.firebaseID) }
    }
}
