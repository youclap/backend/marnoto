import protocol Vapor.Service
import NIO

enum SignUpServiceError: Error {
    case inconsistentFirebaseID(String, String)
    case invalidField(InvalidField)
    case store(Error)

    enum InvalidField {
        case username(String)
        case email(String)
    }
}

protocol SignUpServiceRepresentable: Service {
    func signup(_ signupData: SignUp, authenticationID: String) -> EventLoopFuture<Profile.Response>
}
