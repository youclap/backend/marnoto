import FluentMySQL
import Vapor
import YouClap

struct User {
    var id: ID?
    var name: String
    var username: String
    var email: String
    var createdDate: Date?
    var deleted: Bool = false

    init(id: UnsignedBigInt? = nil,
         name: String,
         username: String,
         email: String,
         createdDate: Date? = nil,
         deleted: Bool = false) {
        self.id = id
        self.name = name
        self.username = username
        self.email = email
        self.createdDate = createdDate
        self.deleted = deleted
    }
}

extension User {
    typealias ID = UnsignedBigInt
}

extension User: MySQLUnsignedBigIntModel {
    static let createdAtKey: TimestampKey? = \.createdDate
}

extension User {
    var profiles: Siblings<User, Profile, UserProfile> {
        return siblings()
    }

    var socialIdentifiers: Children<User, UserSocialIdentifier> {
        return children(\.userID)
    }

    var authenticationIDs: Children<User, AuthenticationIdentifier> {
        return children(\.userID)
    }
}

extension User: Content {}

extension User: CRUDModel {}
