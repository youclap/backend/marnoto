import Vapor
import YouClap

extension Profile {
    struct Response {
        let id: ID
        let name: String
        let username: String
        let type: String
        let avatarURL: String?
        let createdDate: Date?
        let deleted: Bool
    }

    // TODO: Choose a better name instead of `ResponseFromModel`.
    // This is a Response from Model Layer to Service Layer. Ask Portela is this a right way to do...
    struct StoreModel {
        let id: ID
        let name: String
        let username: String
        let typeID: Int
        let typeName: String
        let avatarID: Int
        let createdDate: Date
        let deleted: Bool
        let firebaseIdentifier: String?
        let socialNetworkTypeID: Int?
        let socialNetworkIdentifier: String?
    }
}

extension Profile.Response {
    typealias ID = UnsignedBigInt
}

extension Profile.StoreModel {
    typealias ID = UnsignedBigInt
}

extension Profile.Response: Content {}

extension Profile.StoreModel: Content {}

extension Profile.Response {
    init(profile: Profile, profileType: ProfileType, avatarURL: String? = nil) throws {
        self.id = try profile.requireID()
        self.name = profile.name
        self.username = profile.username
        self.type = profileType.name
        self.avatarURL = avatarURL
        self.createdDate = profile.createdDate
        self.deleted = profile.deleted
    }

    init(id: UnsignedBigInt,
         name: String,
         username: String,
         type: String,
         createdDate: Date,
         deleted: Bool,
         avatarType: AvatarType?,
         firebaseIdentifier: String,
         socialIdentifier: ProfileSocialIdentifier?
         ) throws {

        var avatarURL: String? {
            switch (avatarType, socialIdentifier) {
            case (AvatarType.none, _):
                return nil
            case (AvatarType.local, _):
                return Utils.teatroURL(withIdentifier: firebaseIdentifier)
            case (AvatarType.facebook, let socialIdentifier?):
                return Utils.facebookURL(withIdentifier: socialIdentifier.identifier)
            default:
                assertionFailure("""
                    Avatar Type and Social Identifier does not match!
                    (Maybe the error is Avatar Type is facebook, but does not exists Social Identifier in database!)
                """)
                return nil
            }
        }

        self.id = id
        self.name = name
        self.username = username
        self.type = type
        self.avatarURL = avatarURL
        self.createdDate = createdDate
        self.deleted = deleted
    }
}
