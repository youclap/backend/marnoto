import Vapor

extension Profile {
    struct IsFollowing {
        let isFollowing: Bool
    }

    struct FollowingTotal {
        let count: Int
    }

    struct FollowersTotal {
        let count: Int
    }
}

extension Profile.IsFollowing: Content {}
extension Profile.FollowingTotal: Content {}
extension Profile.FollowersTotal: Content {}
