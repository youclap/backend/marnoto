import Fluent
import YouClap

struct AuthenticationIdentifier {
    var id: UnsignedBigInt?
    var userID: User.ID
    var authenticationID: String

    init(id: UnsignedBigInt? = nil, userID: User.ID, authenticationID: String) {
        self.id = id
        self.userID = userID
        self.authenticationID = authenticationID
    }
}

extension AuthenticationIdentifier: MySQLUnsignedBigIntModel {}

extension AuthenticationIdentifier {
    var user: Parent<AuthenticationIdentifier, User> {
        return parent(\.userID)
    }
}
