import Vapor

struct MexicoFollower {
    let deleted: Bool = false
    let deletedByModerator: Bool = false
    let followedID: String
    let timestamp: Date
    let userID: String
}

extension MexicoFollower: Content {}
