import Vapor

struct MexicoBlockedProfile {
    let blockedID: String
    let timestamp: Date
    let userID: String
}

extension MexicoBlockedProfile: Content {}
