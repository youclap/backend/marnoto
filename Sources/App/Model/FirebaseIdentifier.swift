import Fluent
import YouClap

struct FirebaseIdentifier {
    var id: UnsignedBigInt?
    var profileID: Profile.ID
    var firebaseID: String

    init(id: UnsignedBigInt? = nil, profileID: Profile.ID, firebaseID: String) {
        self.id = id
        self.profileID = profileID
        self.firebaseID = firebaseID
    }
}

extension FirebaseIdentifier: MySQLUnsignedBigIntModel {}

extension FirebaseIdentifier {
    var profile: Parent<FirebaseIdentifier, Profile> {
        return parent(\.profileID)
    }
}
