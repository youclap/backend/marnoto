import FluentMySQL
import Vapor

enum ProfileRole: Int {
    case admin = 0
    case user
}

extension ProfileRole: MySQLModel {
    var id: Int? {
        get {
            switch self {
            case .admin: return 0
            case .user: return 1
            }
        }
        set(newValue) {
            switch newValue {
            case 0?: self = .admin
            case 1?: self = .user
            default: assertionFailure("💥 value \(String(describing: newValue)) not handled.")
            }
        }
    }
}

extension ProfileRole: Codable {
    enum Key: CodingKey {
        case rawValue
    }

    enum CodingError: Error {
        case unknownValue
    }

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: Key.self)
        let rawValue = try container.decode(Int.self, forKey: .rawValue)

        switch rawValue {
        case 0: self = .admin
        case 1: self = .user
        default: throw CodingError.unknownValue
        }
    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: Key.self)
        try container.encode(id, forKey: .rawValue)
    }
}
