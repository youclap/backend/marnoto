import Foundation

struct AmericaUserUsername {
    let email: String
    let userID: String
}

extension AmericaUserUsername: Encodable {}
