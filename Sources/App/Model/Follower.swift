import FluentMySQL
import Vapor

struct Follower {
    var id: Int?
    var followerUserID: User.ID
    var followerProfileID: Profile.ID
    var followingProfileID: Profile.ID
    var createdDate: Date?

    init(id: Int? = nil,
         followerUserID: User.ID,
         followerProfileID: Profile.ID,
         followingProfileID: Profile.ID,
         createdDate: Date? = nil) {
        self.id = id
        self.followerUserID = followerUserID
        self.followerProfileID = followerProfileID
        self.followingProfileID = followingProfileID
        self.createdDate = createdDate
    }
}

extension Follower: MySQLPivot {
    typealias Left = Profile
    typealias Right = Profile

    static var leftIDKey: LeftIDKey = \.followerProfileID
    static var rightIDKey: RightIDKey = \.followingProfileID

    static let createdAtKey: TimestampKey? = \.createdDate

    static var name: String { return "Follower" }
}

extension Follower {
    var follower: Parent<Follower, Profile> {
        return parent(\.followerProfileID)
    }

    var following: Parent<Follower, Profile> {
        return parent(\.followingProfileID)
    }

    var followerUser: Parent<Follower, User> {
        return parent(\.followerUserID)
    }
}
