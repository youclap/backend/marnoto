import FluentMySQL
import Vapor

struct AvatarType {
    var id: Int?
    var name: String
}

extension AvatarType {
    static let none = AvatarType(id: 0, name: "NONE")
    static let local = AvatarType(id: 1, name: "LOCAL")
    static let facebook = AvatarType(id: 2, name: "FACEBOOK")

    init?(name: String) {
        switch name.uppercased() {
        case AvatarType.none.name:
            self = .none
        case AvatarType.local.name:
            self = .local
        case AvatarType.facebook.name:
            self = .facebook
        default:
            assertionFailure("💥 value \(String(describing: name.uppercased())) not handled.")
            return nil
        }
    }

    init?(id: Int) {
        switch id {
        case AvatarType.none.id:
            self = .none
        case AvatarType.local.id:
            self = .local
        case AvatarType.facebook.id:
            self = .facebook
        default:
            assertionFailure("💥 value \(String(describing: id)) not handled.")
            return nil
        }
    }
}

extension AvatarType: MySQLModel {}

extension AvatarType: Equatable {
    static func == (left: AvatarType, right: AvatarType) -> Bool {
        switch (left.id, right.id) {
        case (AvatarType.none.id?, AvatarType.none.id?),
             (AvatarType.local.id?, AvatarType.local.id?),
             (AvatarType.facebook.id?, AvatarType.facebook.id?): return true
        default: return false
        }
    }
}
