import Foundation
import YouClap

struct AmericaDocumentInfo {
    let model: Encodable
    let collection: AmericaMessage.Collection
    let documentID: String

    init<Model: Encodable>(model: Model, collection: AmericaMessage.Collection, documentID: String) {
        self.model = model
        self.collection = collection
        self.documentID = documentID
    }
}
