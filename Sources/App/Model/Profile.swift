import FluentMySQL
import Vapor
import YouClap

struct Profile {
    var id: UnsignedBigInt?
    var name: String
    var username: String
    var typeID: Int
    var avatarID: Int
    var createdDate: Date?
    var deleted: Bool

    init(id: UnsignedBigInt? = nil,
         name: String,
         username: String,
         typeID: Int = ProfileType.user.id!, //swiftlint:disable:this force_unwrapping
         avatarID: Int = AvatarType.none.id!, //swiftlint:disable:this force_unwrapping
         createdDate: Date? = nil,
         deleted: Bool = false) {
        self.id = id
        self.name = name
        self.username = username
        self.typeID = typeID
        self.avatarID = avatarID
        self.createdDate = createdDate
        self.deleted = deleted
    }
}

extension Profile: MySQLUnsignedBigIntModel {
    static let createdAtKey: TimestampKey? = \.createdDate
}

extension Profile {
    var users: Siblings<Profile, User, UserProfile> {
        return siblings()
    }

    var socialIdentifiers: Children<Profile, ProfileSocialIdentifier> {
        return children(\.profileID)
    }

    var avatarType: Parent<Profile, AvatarType> {
        return parent(\.avatarID)
    }

    var profileType: Parent<Profile, ProfileType> {
        return parent(\.typeID)
    }

    var followers: Children<Profile, Follower> {
        return children(\.followerProfileID)
    }

    var followings: Children<Profile, Follower> {
        return children(\.followingProfileID)
    }

    var firebaseIDs: Children<Profile, FirebaseIdentifier> {
        return children(\.profileID)
    }
}

extension Profile: Content {}
