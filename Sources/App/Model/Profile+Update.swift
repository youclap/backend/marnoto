import Vapor

extension Profile {
    enum Update {
        case name(String)
        case avatar(AvatarType)
    }

    struct UpdateContainer {
        let values: [Update]
    }
}

extension Profile.UpdateContainer: RequestDecodable, Decodable {
    static func decode(from req: Request) throws -> EventLoopFuture<Profile.UpdateContainer> {
        return try req.content.decode(Profile.UpdateContainer.self)
    }

    enum Key: String, CodingKey {
        case name
        case avatar
    }

    enum DecodingError: Error {
        case unknownValue
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: Key.self)

        self.values = try values.allKeys.map {
            switch $0 {
            case .name: return Profile.Update.name(try values.decode(String.self, forKey: $0))
            case .avatar:
                let avatarTypeAsString = try values.decode(String.self, forKey: $0)
                guard let avatar = AvatarType(name: avatarTypeAsString) else {
                    throw DecodingError.unknownValue
                }
                return Profile.Update.avatar(avatar)
            }
        }
    }
}
