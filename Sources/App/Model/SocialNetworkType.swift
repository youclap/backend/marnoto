import FluentMySQL

struct SocialNetworkType {
    var id: Int?
    var name: String
}

extension SocialNetworkType: MySQLModel {}

extension SocialNetworkType {
    static let facebook = SocialNetworkType(id: 0, name: "FACEBOOK")
}
