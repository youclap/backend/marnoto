import FluentMySQL
import Foundation
import Vapor

extension BlockedProfile {
    struct Create {
        var userID: User.ID
        var profileID: Profile.ID
        var blockedProfileID: Profile.ID
    }
}

extension BlockedProfile.Create: Content {
    enum Key: CodingKey {
        case userID
        case profileID
        case blockedProfileID
    }

    enum CodingError: Error {
        case unknownValue
    }

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: Key.self)

        self.userID = try container.decode(User.ID.self, forKey: .userID)
        self.profileID = try container.decode(Profile.ID.self, forKey: .profileID)
        self.blockedProfileID = try container.decode(Profile.ID.self, forKey: .blockedProfileID)
    }
}
