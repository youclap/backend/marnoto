import Vapor

extension User {
    struct Create {
        var name: String
        var username: String
        var email: String
        var authenticationID: String
    }

    enum Update {
        case name(String)
    }

    struct UpdateContainer {
        let values: [Update]
    }

    struct ReplaceContainer {
        let values: [Update]
    }
}

extension User.Update {
    enum CodingKeys: String, CodingKey {
        case name
    }
}

extension User.Create: Content {}

extension User.UpdateContainer: Content {
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: User.Update.CodingKeys.self)

        self.values = try values.allKeys.map {
            switch $0 {
            case .name: return User.Update.name(try values.decode(String.self, forKey: $0))
            }
        }
    }

    func encode(to encoder: Encoder) throws {
        fatalError("💥 unimplemented")
    }
}

extension User.ReplaceContainer: Content {
    // TODO: Remove this!
    // THIS SHOULD REPLACE ALL THE OBJECT AND NOT ONLY SOME FIELDS.
    // PUT != PATCH
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: User.Update.CodingKeys.self)

        self.values = try values.allKeys.map {
            switch $0 {
            case .name: return User.Update.name(try values.decode(String.self, forKey: $0))
            }
        }
    }

    func encode(to encoder: Encoder) throws {
        fatalError("💥 unimplemented")
    }
}
