import Vapor

struct MexicoUser {
    let displayName: String
    let email: String
    let facebookID: String?
    let photoURL: String?
    let timestamp: Date
    let uid: String
    let username: String
    let deleted: Bool = false
}

extension MexicoUser: Content {}
