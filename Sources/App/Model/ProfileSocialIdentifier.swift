import FluentMySQL

struct ProfileSocialIdentifier {
    var id: Int?
    var profileID: Profile.ID
    var socialNetworkTypeID: Int
    var identifier: String

    init(id: Int? = nil, profileID: Profile.ID, socialNetworkTypeID: Int, identifier: String) {
        self.id = id
        self.profileID = profileID
        self.socialNetworkTypeID = socialNetworkTypeID
        self.identifier = identifier
    }

    init?(id: Int? = nil, profileID: Profile.ID? = nil, socialNetworkTypeID: Int? = nil, identifier: String? = nil) {
        guard
            let profileID = profileID,
            let socialNetworkTypeID = socialNetworkTypeID,
            let identifier = identifier
        else {
                return nil
        }

        self.id = id
        self.profileID = profileID
        self.socialNetworkTypeID = socialNetworkTypeID
        self.identifier = identifier
    }
}

extension ProfileSocialIdentifier {
    var socialNetworkType: Parent<ProfileSocialIdentifier, SocialNetworkType> {
        return parent(\.socialNetworkTypeID)
    }

    var profile: Parent<ProfileSocialIdentifier, Profile> {
        return parent(\.profileID)
    }
}

extension ProfileSocialIdentifier: MySQLModel {}
