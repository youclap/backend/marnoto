import FluentMySQL
import Vapor

struct BlockedProfile {
    var id: Int?
    var userID: User.ID
    var profileID: Profile.ID
    var blockedProfileID: Profile.ID
    var createdDate: Date?

    init(id: Int? = nil,
         userID: User.ID,
         profileID: Profile.ID,
         blockedProfileID: Profile.ID,
         createdDate: Date? = nil) {
        self.id = id
        self.userID = userID
        self.profileID = profileID
        self.blockedProfileID = blockedProfileID
        self.createdDate = createdDate
    }
}

extension BlockedProfile: MySQLPivot {
    typealias Left = Profile
    typealias Right = Profile

    static var leftIDKey: LeftIDKey = \.profileID
    static var rightIDKey: RightIDKey = \.blockedProfileID

    static let createdAtKey: TimestampKey? = \.createdDate

    static var name: String { return "BlockedProfile" }
}

extension BlockedProfile {
    var blocking: Parent<BlockedProfile, Profile> {
        return parent(\.profileID)
    }

    var blocked: Parent<BlockedProfile, Profile> {
        return parent(\.blockedProfileID)
    }

    var blockingUser: Parent<BlockedProfile, User> {
        return parent(\.userID)
    }
}
