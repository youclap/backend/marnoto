import Vapor

extension MexicoUser {
    enum Update {
        case displayName(String)
        case email(String)
        case facebookID(String?)
        case photoURL(String?)
        case timestamp(Date)
        case uid(String)
        case username(String)
        case deleted(Bool)
    }

    struct UpdateContainer {
        let values: [Update]
    }
}

extension MexicoUser.UpdateContainer: RequestDecodable, Decodable {
    static func decode(from req: Request) throws -> EventLoopFuture<MexicoUser.UpdateContainer> {
        return try req.content.decode(MexicoUser.UpdateContainer.self)
    }

    enum Key: String, CodingKey {
        case displayName
        case email
        case facebookID
        case photoURL
        case deleted
    }

    enum DecodingError: Error {
        case unknownValue
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: Key.self)

        self.values = try values.allKeys.map {
            switch $0 {
            case .displayName: return MexicoUser.Update.displayName(try values.decode(String.self, forKey: $0))
            case .email: return MexicoUser.Update.email(try values.decode(String.self, forKey: $0))
            case .facebookID: return MexicoUser.Update.facebookID(try values.decode(String.self, forKey: $0))
            case .photoURL: return MexicoUser.Update.photoURL(try values.decode(String.self, forKey: $0))
            case .deleted: return MexicoUser.Update.deleted(try values.decode(Bool.self, forKey: $0))
            }
        }
    }
}
