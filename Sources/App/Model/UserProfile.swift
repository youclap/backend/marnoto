import FluentMySQL
import Vapor

struct UserProfile {
    var id: Int?
    var userID: User.ID
    var profileID: Profile.ID
    var addedByUserID: User.ID
    var roleID: Int
    var createdDate: Date?
}

extension UserProfile: MySQLPivot {
    typealias Left = User
    typealias Right = Profile

    static var leftIDKey: LeftIDKey = \.userID
    static var rightIDKey: RightIDKey = \.profileID

    static let createdAtKey: TimestampKey? = \.createdDate

    static var name: String { return "User_Profile" }
}

extension UserProfile {
    var profileRole: Parent<UserProfile, ProfileRole> {
        return parent(\.roleID)
    }

    var profile: Parent<UserProfile, Profile> {
        return parent(\.profileID)
    }

    var user: Parent<UserProfile, User> {
        return parent(\.userID)
    }
}

extension UserProfile: ModifiablePivot {
    init(_ left: User, _ right: Profile) throws {
        self.userID = try left.requireID()
        self.profileID = try right.requireID()
        self.addedByUserID = try left.requireID()
        self.roleID = ProfileRole.admin.rawValue
    }
}
