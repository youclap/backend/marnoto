import FluentMySQL
import Foundation
import Vapor

extension Profile {
    struct Create {
        var name: String
        var username: String
        var type: ProfileType
        var avatar: AvatarType
    }
}

extension Profile.Create: Content {
    enum Key: CodingKey {
        case name
        case username
        case type
        case avatar
        case facebookID
    }

    enum CodingError: Error {
        case unknownValue
    }

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: Key.self)

        let typeAsString = try container.decode(String.self, forKey: .type)
        guard let profileType = ProfileType(name: typeAsString) else {
            throw CodingError.unknownValue
        }

        let avatarTypeAsString = try container.decode(String.self, forKey: .avatar)
        guard let avatarType = AvatarType(name: avatarTypeAsString) else {
            throw CodingError.unknownValue
        }

        self.name = try container.decode(String.self, forKey: .name)
        self.username = try container.decode(String.self, forKey: .username)
        self.type = profileType
        self.avatar = avatarType
    }
}
