import FluentMySQL
import Foundation
import Vapor

extension Follower {
    struct Create {
        var followerUserID: User.ID
        var followerProfileID: Profile.ID
        var followingProfileID: Profile.ID
    }
}

extension Follower.Create: Content {
    enum Key: CodingKey {
        case followerUserID
        case followerProfileID
        case followingProfileID
    }

    enum CodingError: Error {
        case unknownValue
    }

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: Key.self)

        self.followerUserID = try container.decode(User.ID.self, forKey: .followerUserID)
        self.followerProfileID = try container.decode(Profile.ID.self, forKey: .followerProfileID)
        self.followingProfileID = try container.decode(Profile.ID.self, forKey: .followingProfileID)
    }
}
