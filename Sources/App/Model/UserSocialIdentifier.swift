import FluentMySQL

struct UserSocialIdentifier {
    var id: Int?
    var userID: User.ID
    var socialNetworkTypeID: Int
    var identifier: String

    init(id: Int? = nil, userID: User.ID, socialNetworkTypeID: Int, identifier: String) {
        self.id = id
        self.userID = userID
        self.socialNetworkTypeID = socialNetworkTypeID
        self.identifier = identifier
    }
}

extension UserSocialIdentifier {
    var socialNetworkType: Parent<UserSocialIdentifier, SocialNetworkType> {
        return parent(\.socialNetworkTypeID)
    }

    var user: Parent<UserSocialIdentifier, User> {
        return parent(\.userID)
    }
}

extension UserSocialIdentifier: MySQLModel {}
