import Vapor

struct SignUp {
    let name: String
    let username: String
    let email: String
    let firebaseID: String
    let type: ProfileType
    let avatar: AvatarType

    let facebookID: String?
}

extension SignUp: Content {
    enum Key: CodingKey {
        case name
        case username
        case email
        case firebaseID
        case type
        case avatar
        case facebookID
    }

    enum CodingError: Error {
        case unknownValue
    }

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: Key.self)

        let typeAsString = try container.decode(String.self, forKey: .type)
        guard let profileType = ProfileType(name: typeAsString) else {
            throw CodingError.unknownValue
        }

        let avatarTypeAsString = try container.decode(String.self, forKey: .avatar)
        guard let avatarType = AvatarType(name: avatarTypeAsString) else {
            throw CodingError.unknownValue
        }

        self.name = try container.decode(String.self, forKey: .name)
        self.username = try container.decode(String.self, forKey: .username)
        self.email = try container.decode(String.self, forKey: .email)
        self.firebaseID = try container.decode(String.self, forKey: .firebaseID)
        self.type = profileType
        self.avatar = avatarType

        self.facebookID = try container.decodeIfPresent(String.self, forKey: .facebookID)
    }
}
