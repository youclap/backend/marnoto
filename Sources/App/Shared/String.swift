import Foundation

extension String {
    func trim() -> Self {
        return trimmingCharacters(in: .whitespacesAndNewlines)
    }

    func matches(_ regexp: String) -> Bool {
        return range(of: regexp, options: .regularExpression) != nil
    }
}
