import Foundation

extension String {
    private static let letters = """
        abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789±§!@#$%^&*()+|/?💩🤮🤢🤡👹🔨🦷👎
    """

    static func random(with length: Int) -> String {
        // swiftlint:disable:next force_unwrapping
        return "🗑\(String((0..<length).map { _ in letters.randomElement()! }))"
    }
}
