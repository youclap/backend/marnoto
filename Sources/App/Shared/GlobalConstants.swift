import Vapor

enum GlobalConstants {
    static let facebookEndpoint = "https://graph.facebook.com/%s/picture?type=large"

    enum FieldSize {
        static let username = 250
        static let email = 250
        static let name = 250
        static let authenticationID = 29
    }
}
