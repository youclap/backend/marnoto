import Vapor

enum Utils {
    static func facebookURL(withIdentifier identifier: String) -> String {
        return identifier.withCString {
            String(format: GlobalConstants.facebookEndpoint, $0)
        }
    }

    static func teatroURL(withIdentifier identifier: String) -> String {
        guard let teatroEndpoint = Environment.get("ENDPOINT_TEATRO") else {
            fatalError("💥 missing teatro endpoint")
        }

        return "\(teatroEndpoint)/user/\(identifier)"
    }
}
