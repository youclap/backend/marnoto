import Fluent
import YouClap

enum MexicoStoreError: Error {
    case notFound(String)
    case sql(Error)
}

protocol MexicoStore: Service {
    func create(user: User,
                profile: Profile,
                facebookID: String?,
                firebaseIdentifier: String) -> Future<(User, Profile)>
    func update(firebaseIdentifier: String,
                facebookID: String?,
                updateUser: @escaping (User) -> User,
                updateProfile: @escaping (Profile) throws -> (Profile, String?)) -> Future<(User, Profile)>
    func delete(user: User, profile: Profile) -> Future<(User, Profile)>

    func createFollower(mexicoFollower: MexicoFollower) -> Future<Follower>
    func user(_ userID: Profile.ID, unfollow followedID: Profile.ID) -> Future<Follower?>

    func createBlockedProfile(mexicoBlockedProfile: MexicoBlockedProfile) -> Future<BlockedProfile>
    func user(_ userID: Profile.ID, unblock blockedID: Profile.ID) -> Future<BlockedProfile?>

    func user(withAuthenticationID authenticationID: String) -> Future<User>
    func profile(withFirebaseID firebaseID: String) -> Future<Profile>
}

extension Store: MexicoStore {
    func create(user: User,
                profile: Profile,
                facebookID: String?,
                firebaseIdentifier: String) -> Future<(User, Profile)> {
        return databaseConnectable.transaction { connection in
            user.save(on: connection)
                .flatMap { user -> Future<User> in
                    let authenticationIdentifier = AuthenticationIdentifier(userID: try user.requireID(),
                                                                            authenticationID: firebaseIdentifier)
                    return authenticationIdentifier.save(on: connection).map { _ in user }
                }
                .flatMap { user in profile.save(on: connection).map { (user, $0) } }
                .flatMap { user, profile -> Future<(User, Profile)> in
                    let firebaseIdentifier = FirebaseIdentifier(profileID: try profile.requireID(),
                                                                firebaseID: firebaseIdentifier)

                    return firebaseIdentifier.save(on: connection).map { _ in (user, profile) }
                }
                .flatMap { user, profile -> Future<(User, Profile)> in
                    user.profiles.attach(profile, on: connection).map { _ in (user, profile) }
                }
                .flatMap { user, profile -> Future<(User, Profile)> in
                    guard let facebookID = facebookID else {
                        return connection.eventLoop.future((user, profile))
                    }

                    let socialNetworkType = SocialNetworkType.facebook

                    let userSocialIdentifier = UserSocialIdentifier(
                        userID: try user.requireID(),
                        socialNetworkTypeID: try socialNetworkType.requireID(),
                        identifier: facebookID)

                    let profileSocialIdentifier = ProfileSocialIdentifier(
                        profileID: try profile.requireID(),
                        socialNetworkTypeID: try socialNetworkType.requireID(),
                        identifier: facebookID)

                    return userSocialIdentifier.save(on: connection)
                        .flatMap { _ in profileSocialIdentifier.save(on: connection) }
                        .map { _ in (user, profile) }
                }
        }
    }

    func update(firebaseIdentifier: String,
                facebookID: String?,
                updateUser: @escaping (User) -> User,
                updateProfile: @escaping (Profile) throws -> (Profile, String?)) -> Future<(User, Profile)> {
        return databaseConnectable.transaction { connection in
            self.userAndProfile(withAuthenticationID: firebaseIdentifier, on: connection)
                .flatMap { user, profile -> Future<(User, Profile)> in
                    let newUser = updateUser(user)
                    let (newProfile, photoFacebookID) = try updateProfile(profile)

                    return newUser.update(on: connection)
                        .flatMap { user in newProfile.update(on: connection).map { (user, $0) } }
                        .flatMap { user, profile -> Future<(User, Profile)> in
                            guard let facebookID = (facebookID ?? photoFacebookID) else {
                                return connection.eventLoop.future((user, profile))
                            }

                            let socialNetworkType = SocialNetworkType.facebook

                            // swiftlint:disable:next first_where
                            let userSocialIdentifier = UserSocialIdentifier.query(on: connection)
                                .filter(\.userID == (try user.requireID()))
                                .first()
                                .flatMap { socialIdentifier -> Future<UserSocialIdentifier> in
                                    if let socialIdentifier = socialIdentifier {
                                        return connection.eventLoop.newSucceededFuture(result: socialIdentifier)
                                    }

                                    let userSocialIdentifier = UserSocialIdentifier(
                                        userID: try user.requireID(),
                                        socialNetworkTypeID: try socialNetworkType.requireID(),
                                        identifier: facebookID)

                                    return userSocialIdentifier.save(on: connection)
                                }

                            // swiftlint:disable:next first_where
                            let profileSocialIdentifier = ProfileSocialIdentifier.query(on: connection)
                                .filter(\.profileID == (try profile.requireID()))
                                .first()
                                .flatMap { socialIdentifier -> Future<ProfileSocialIdentifier> in
                                    if let socialIdentifier = socialIdentifier {
                                        return connection.eventLoop.newSucceededFuture(result: socialIdentifier)
                                    }

                                    let profileSocialIdentifier = ProfileSocialIdentifier(
                                        profileID: try profile.requireID(),
                                        socialNetworkTypeID: try socialNetworkType.requireID(),
                                        identifier: facebookID)

                                    return profileSocialIdentifier.save(on: connection)
                                }

                            return userSocialIdentifier.flatMap { _ in profileSocialIdentifier }
                                .map { _ in (user, profile) }
                        }
                }
        }
    }

    func delete(user: User, profile: Profile) -> Future<(User, Profile)> {
        return databaseConnectable.transaction { connection in
            try profile.users.pivots(on: connection).delete(force: true)
                .flatMap { _ in try profile.socialIdentifiers.query(on: connection).delete(force: true) }
                .flatMap { _ in try profile.followers.query(on: connection).delete(force: true) }
                .flatMap { _ in try profile.followings.query(on: connection).delete(force: true) }
                .flatMap { _ in try profile.firebaseIDs.query(on: connection).delete(force: true) }
                .flatMap { _ in try user.socialIdentifiers.query(on: connection).delete(force: true) }
                .flatMap { _ in try user.authenticationIDs.query(on: connection).delete(force: true) }
                .flatMap { _ in user.update(on: connection) }
                .flatMap { user in profile.update(on: connection).map { (user, $0) } }
        }
    }

    func createFollower(mexicoFollower: MexicoFollower) -> Future<Follower> {
        return databaseConnectable.transaction { connection in
            self.profile(withFirebaseID: mexicoFollower.userID, on: connection)
                .flatMap { profileA in
                    self.profile(withFirebaseID: mexicoFollower.followedID, on: connection)
                        .map { profileB  in (profileA, profileB) }
                }
                .flatMap { profileA, profileB -> Future<(Profile, Profile, User)> in
                    try profileA.users.query(on: connection)
                        .first()
                        .map {
                            guard let user = $0 else {
                                throw MexicoStoreError.notFound("User not found")
                            }
                            return (profileA, profileB, user)
                        }
                }
                .flatMap { profileA, profileB, user -> Future<Follower> in
                    let newFollower = Follower(id: nil,
                                               followerUserID: try user.requireID(),
                                               followerProfileID: try profileA.requireID(),
                                               followingProfileID: try profileB.requireID(),
                                               createdDate: mexicoFollower.timestamp)
                    return newFollower.save(on: connection)
                }
        }
    }

    func user(_ userID: Profile.ID, unfollow followedID: Profile.ID) -> Future<Follower?> {
        return databaseConnectable.connection { connection in
            // swiftlint:disable:next first_where
            Follower.query(on: connection)
                .filter(\Follower.followerProfileID == userID)
                .filter(\Follower.followingProfileID == followedID)
                .first()
        }
    }

    func createBlockedProfile(mexicoBlockedProfile: MexicoBlockedProfile) -> Future<BlockedProfile> {
        return databaseConnectable.transaction { connection in
            self.profile(withFirebaseID: mexicoBlockedProfile.userID, on: connection)
                    .flatMap { profileA in
                        self.profile(withFirebaseID: mexicoBlockedProfile.blockedID, on: connection)
                            .map { profileB in (profileA, profileB) }
                    }
                    .flatMap { profileA, profileB -> Future<(Profile, Profile, User)> in
                        try profileA.users.query(on: connection)
                            .first()
                            .map {
                                guard let user = $0 else {
                                    throw MexicoStoreError.notFound("User not found")
                                }
                                return (profileA, profileB, user)
                            }
                    }
                    .flatMap { profileA, profileB, user -> Future<BlockedProfile> in
                        let newBlockedProfile =
                                BlockedProfile(id: nil,
                                               userID: try user.requireID(),
                                               profileID: try profileA.requireID(),
                                               blockedProfileID: try profileB.requireID(),
                                               createdDate: mexicoBlockedProfile.timestamp)
                        return newBlockedProfile.save(on: connection)
                    }
        }
    }

    func user(_ userID: Profile.ID, unblock blockedID: Profile.ID) -> EventLoopFuture<BlockedProfile?> {
        return databaseConnectable.connection { connection in
            // swiftlint:disable:next first_where
            BlockedProfile.query(on: connection)
                .filter(\BlockedProfile.profileID == userID)
                .filter(\BlockedProfile.blockedProfileID == blockedID)
                .first()
        }
    }

    func user(withAuthenticationID authenticationID: String) -> EventLoopFuture<User> {
        return databaseConnectable.connection {
            self.user(withAuthenticationID: authenticationID, on: $0)
        }
    }

    func profile(withFirebaseID firebaseID: String) -> EventLoopFuture<Profile> {
        return databaseConnectable.connection {
            self.profile(withFirebaseID: firebaseID, on: $0)
        }
    }

    // MARK: - Private Methods

    private func user(withAuthenticationID authenticationID: String, on connection: DatabaseConnectable)
    -> Future<User> {
        return AuthenticationIdentifier.query(on: connection)
            .join(\User.id, to: \AuthenticationIdentifier.userID, method: .inner)
            .filter(\.authenticationID == authenticationID)
            .alsoDecode(User.self)
            .first()
            .map {
                guard let user = $0?.1 else {
                    throw MexicoStoreError.notFound(authenticationID)
                }
                return user
            }
    }

    private func profile(withFirebaseID firebaseID: String, on connection: DatabaseConnectable)
    -> Future<Profile> {
        return FirebaseIdentifier.query(on: connection)
            .join(\Profile.id, to: \FirebaseIdentifier.profileID, method: .inner)
            .filter(\.firebaseID == firebaseID)
            .alsoDecode(Profile.self)
            .first()
            .map {
                guard let profile = $0?.1 else {
                    throw MexicoStoreError.notFound(firebaseID)
                }
                   return profile
            }
    }

    private func userAndProfile(withAuthenticationID authenticationID: String, on connection: DatabaseConnectable)
    -> Future<(User, Profile)> {
        return AuthenticationIdentifier.query(on: connection)
            .join(\User.id, to: \AuthenticationIdentifier.userID, method: .inner)
            .filter(\.authenticationID == authenticationID)
            .alsoDecode(User.self)
            .first()
            .map { authUser -> User in
                guard let user = authUser?.1 else {
                    throw MexicoStoreError.notFound(authenticationID)
                }
                return user
            }
            .flatMap { user -> Future<(User, Profile)> in
                try user.profiles.query(on: connection)
                    .filter(\Profile.typeID == ProfileType.user.id!) //swiftlint:disable:this force_unwrapping
                    .first()
                    .map {
                        guard let profile = $0 else {
                            throw MexicoStoreError.notFound(authenticationID)
                        }

                        return (user, profile)
                    }
            }
    }
}
