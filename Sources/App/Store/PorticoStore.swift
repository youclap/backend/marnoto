import YouClap

protocol PorticoStore: Service {
    func updateClaims(firebaseID: String, userID: User.ID, profileID: Profile.ID) -> Future<Void>
}

extension Store: PorticoStore {
    func updateClaims(firebaseID: String, userID: User.ID, profileID: Profile.ID) -> Future<Void> {
        let url = "http://portico/updateclaims"

        let queryItems = [
            URLQueryItem(name: "authenticationID", value: firebaseID),
            URLQueryItem(name: "userID", value: "\(userID)"),
            URLQueryItem(name: "profileID", value: "\(profileID)")
        ]

        return httpClientConnectable.get(url: url, queryItems: queryItems).map { _ in () }
    }
}
