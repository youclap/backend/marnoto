import Fluent
import Vapor
import YouClap

protocol UserStore: CRUDStore where M == User {
    func create(_ user: User, withAuthenticationID authenticationID: String) -> EventLoopFuture<User>
    func user(by authenticationID: String) -> EventLoopFuture<User>
    func user(for profileID: Profile.ID) -> EventLoopFuture<User>
}

extension Store: UserStore {
    func create(_ user: User, withAuthenticationID authenticationID: String) -> EventLoopFuture<User> {
        return databaseConnectable.transaction { connection in
            self.create(user, withAuthenticationID: authenticationID, on: connection)
        }
    }

    func create(_ user: User,
                withAuthenticationID authenticationID: String,
                on connection: MySQLConnection) -> EventLoopFuture<User> {
        return user.save(on: connection).flatMap { user in
            let authenticationIdentifier = AuthenticationIdentifier(userID: try user.requireID(),
                                                                    authenticationID: authenticationID)

            return authenticationIdentifier.save(on: connection).map { _ in user }
        }
    }

    func user(by authenticationID: String) -> EventLoopFuture<User> {
        return databaseConnectable.connection {
            AuthenticationIdentifier.query(on: $0)
                .filter(\.authenticationID == authenticationID)
                .join(\User.id, to: \AuthenticationIdentifier.userID, method: .inner)
                .alsoDecode(User.self)
                .first()
                .map {
                    guard let user = $0?.1 else {
                        throw CRUDStoreError.notFound
                    }
                    return user
                }
        }
    }

    func user(for profileID: Profile.ID) -> EventLoopFuture<User> {
        return databaseConnectable.connection { conn in
            // swiftlint:disable:next first_where
            UserProfile.query(on: conn)
                .filter(\.profileID == profileID)
                .first()
                .flatMap { userProfile in
                    guard let user = userProfile?.user.get(on: conn) else {
                        throw CRUDStoreError.notFound
                    }
                    return user
                }
        }
    }

}
