import Vapor
import YouClap

enum ElasticSearchStoreError: Error {
    case generic(Error)
}

protocol ElasticSearchStore {
    func index(profile: Profile) throws -> Future<Void>
    func unindex(profileID: Profile.ID) throws -> Future<Void>
}

extension Store: ElasticSearchStore {
    func index(profile: Profile) throws -> Future<Void> {
        let path = try "/profile/_doc/\(profile.requireID())"

        return elasticSearchConnectable.post(path, content: profile)
            .mapError(ElasticSearchStoreError.generic)
            .map {
                print("elastic search response \($0)")

                return ()
            }
    }

    func unindex(profileID: Profile.ID) throws -> EventLoopFuture<Void> {
        let path = "/profile/_doc/\(profileID)"

        return elasticSearchConnectable.delete(path)
            .mapError(ElasticSearchStoreError.generic)
            .map {
                print("elastic search response \($0)")

                return ()
            }
    }
}
