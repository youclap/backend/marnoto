import Fluent
import Vapor
import YouClap

enum ProfileStoreError: Error {
    case notFoundProfileForUser(User)
    case notFoundFirebaseIdentifier(Profile.ID)
    case notFoundProfileUserForProfile(Profile)
    case unknownProfileID(Profile.ID)
    case unknownProfileWithUsername(String)
    case sql(Error)
}

protocol ProfileStore {
    func create(profile: Profile, for user: User, firebaseIdentifier: String) -> Future<Profile>
    func profile(for id: Profile.ID) -> Future<Profile>
    func profile(for id: Profile.ID, authenticatedID: Profile.ID) -> Future<Profile>
    func profile(for user: User) -> Future<Profile>
    func profile(for username: String) -> Future<Profile>
    func profile(for username: String, authenticatedID: Profile.ID) -> Future<Profile>
    func update(profile: Profile) -> EventLoopFuture<Profile>
    func update(profile: Profile, and updateUser: @escaping (User) -> User) -> Future<(Profile, User)>
    func delete(profile: Profile, user: User) -> Future<(Profile, User)>

    func profiles(for ids: [Profile.ID]) -> Future<[Profile]>
    func profiles(for ids: [Profile.ID], authenticatedProfileID: Profile.ID) -> Future<[Profile]>

    func followers(for id: Profile.ID, andFor requestProfileID: Profile.ID) -> Future<[Profile.StoreModel]>
    func following(for id: Profile.ID, andFor requestProfileID: Profile.ID) -> Future<[Profile.StoreModel]>
    func startFollowing(follower: Follower) -> Future<Follower>
    func unfollow(follower: Follower) -> Future<Void>
    func isFollowing(from fromProfileID: Profile.ID, to toProfileID: Profile.ID) -> Future<Bool>
    func followingTotal(profileID: Profile.ID) -> Future<Int>
    func followersTotal(profileID: Profile.ID) -> Future<Int>
    func friends(for id: Profile.ID) -> Future<[Profile.StoreModel]>
    func user(_ user: User, canReadProfile profile: Profile) -> Future<Bool>

    func socialIdentifier(for profile: Profile) -> Future<ProfileSocialIdentifier?>
    func firebaseIdentifier(for profile: Profile) -> Future<FirebaseIdentifier>
    func profileType(for profile: Profile) -> Future<ProfileType>
    func avatarType(for profile: Profile) -> Future<AvatarType>

    func blocked(for id: Profile.ID) -> Future<[Profile.StoreModel]>

    func firebaseIdentifier(for profileID: Profile.ID) -> Future<FirebaseIdentifier>

    func blockProfile(blockedProfile: BlockedProfile) -> Future<BlockedProfile>
    func unblockProfile(blockedProfile: BlockedProfile) -> Future<Void>
}

extension Store: ProfileStore {
    func create(profile: Profile, for user: User, firebaseIdentifier: String) -> Future<Profile> {
        return databaseConnectable.transaction { connection in
            self.create(profile: profile, for: user, firebaseIdentifier: firebaseIdentifier, on: connection)
        }
    }

    func create(profile: Profile, for user: User, firebaseIdentifier: String, on connection: MySQLConnection)
    -> Future<Profile> {
        return profile.save(on: connection).flatMap { profile -> Future<Profile> in
            user.profiles.attach(profile, on: connection).map { _ in profile }
        }
        .flatMap { profile in
            let firebaseIdentifier = FirebaseIdentifier(profileID: try profile.requireID(),
                                                        firebaseID: firebaseIdentifier)

            return firebaseIdentifier.save(on: connection).map { _ in profile }
        }
    }

    func profile(for id: Profile.ID) -> Future<Profile> {
        return databaseConnectable.connection {
            Profile.find(id, on: $0)
                .map { profile -> Profile in
                    guard let profile = profile else {
                        throw ProfileStoreError.unknownProfileID(id)
                    }

                    return profile
                }
        }
    }

    func profile(for id: Profile.ID, authenticatedID: Profile.ID) -> Future<Profile> {
        return databaseConnectable.connection { connection in
            connection.raw("""
            SELECT *
            FROM Profile
            WHERE id = ?
                AND id NOT IN (
                    SELECT profileID FROM BlockedProfile WHERE blockedProfileID = ?
                    UNION
                    SELECT blockedProfileID FROM BlockedProfile WHERE profileID = ?
                )
                AND deleted = 0
            """)
                .binds([id, authenticatedID, authenticatedID])
                .first(decoding: Profile.self)
                .map { profile -> Profile in
                    guard let profile = profile else {
                        throw ProfileStoreError.unknownProfileID(id)
                    }

                    return profile
                }
        }
    }

    func profile(for user: User) -> Future<Profile> {
        return databaseConnectable.connection {
            try user.profiles.query(on: $0)
                .filter(\.typeID == ProfileType.user.id!) //swiftlint:disable:this force_unwrapping
                .first()
                .map {
                    guard let profile = $0 else {
                        throw ProfileStoreError.notFoundProfileForUser(user)
                    }

                    return profile
                }
        }
    }

    func profile(for username: String) -> Future<Profile> {
        return databaseConnectable.connection {
            // swiftlint:disable:next first_where
            Profile.query(on: $0)
                .filter(\.username == username)
                .first()
                .map {
                    guard let profile = $0 else {
                        throw ProfileStoreError.unknownProfileWithUsername(username)
                    }

                    return profile
                }
        }
    }

    func profile(for username: String, authenticatedID: Profile.ID) -> Future<Profile> {
        return databaseConnectable.connection { connection in
            connection.raw("""
            SELECT *
            FROM Profile
            WHERE username = ?
                AND id NOT IN (
                    SELECT profileID FROM BlockedProfile WHERE blockedProfileID = ?
                    UNION
                    SELECT blockedProfileID FROM BlockedProfile WHERE profileID = ?
                )
                AND deleted = 0
            """)
                .binds([username, authenticatedID, authenticatedID])
                .first(decoding: Profile.self)
                .thenIfErrorThrowing { error in
                    throw error
                }
                .map {
                    guard let profile = $0 else {
                        throw ProfileStoreError.unknownProfileWithUsername(username)
                    }

                    return profile
                }
        }
    }

    private func update(profile: Profile, on connection: MySQLConnection) -> EventLoopFuture<Profile> {
        return profile.update(on: connection)
    }

    func update(profile: Profile) -> EventLoopFuture<Profile> {
        return databaseConnectable.connection { self.update(profile: profile, on: $0) }
    }

    func update(profile: Profile, and updateUser: @escaping (User) -> User) -> Future<(Profile, User)> {
        return databaseConnectable.transaction { conn in
            self.update(profile: profile, on: conn)
                .flatMap { profile in
                    try profile.users.query(on: conn).first().flatMap { user in
                        guard let user = user else {
                            throw ProfileStoreError.notFoundProfileUserForProfile(profile)
                        }

                        let userUpdated = updateUser(user)

                        return userUpdated.update(on: conn).map { (profile, $0) }
                    }
                }
        }
    }

    func delete(profile: Profile, user: User) -> EventLoopFuture<(Profile, User)> {
        return databaseConnectable.transaction { connection in
            try profile.users.pivots(on: connection).delete(force: true)
                .flatMap { _ in try profile.socialIdentifiers.query(on: connection).delete(force: true) }
                .flatMap { _ in try profile.followers.query(on: connection).delete(force: true) }
                .flatMap { _ in try profile.followings.query(on: connection).delete(force: true) }
                .flatMap { _ in try profile.firebaseIDs.query(on: connection).delete(force: true) }
                .flatMap { _ in try user.socialIdentifiers.query(on: connection).delete(force: true) }
                .flatMap { _ in try user.authenticationIDs.query(on: connection).delete(force: true) }
                .flatMap { _ in profile.update(on: connection) }
                .flatMap { profile in user.update(on: connection).map { (profile, $0) } }
        }
    }

    func profiles(for ids: [Profile.ID]) -> Future<[Profile]> {
        return databaseConnectable.connection { connection in
            let profiles = ids.map { "\($0)" }.joined(separator: ",")
            let profilesAsArguments = ids.map { _ in "?" }.joined(separator: ",")

            return connection.raw("""
            SELECT * FROM Profile
            WHERE id IN (\(profilesAsArguments))
            ORDER BY FIELD(id, \(profiles))
            """)
                .binds(ids)
                .all(decoding: Profile.self)
        }
    }

    func profiles(for ids: [Profile.ID], authenticatedProfileID: Profile.ID) -> Future<[Profile]> {
        return databaseConnectable.connection { connection in
            let profiles = ids.map { "\($0)" }.joined(separator: ",")
            let profilesAsArguments = ids.map { _ in "?" }.joined(separator: ",")

            let ids = ids + [authenticatedProfileID, authenticatedProfileID]

            return connection.raw("""
            SELECT * FROM Profile
            WHERE id IN (\(profilesAsArguments))
            AND id NOT IN (
                SELECT profileID FROM BlockedProfile WHERE blockedProfileID = ?
                UNION
                SELECT blockedProfileID FROM BlockedProfile WHERE profileID = ?
            )
            AND deleted = 0
            ORDER BY FIELD(id, \(profiles))
            """)
                .binds(ids)
                .all(decoding: Profile.self)
        }
    }

    func user(_ user: User, canReadProfile profile: Profile) -> Future<Bool> {
        return databaseConnectable.connection {
            user.profiles.isAttached(profile, on: $0)
        }
    }

    func socialIdentifier(for profile: Profile) -> EventLoopFuture<ProfileSocialIdentifier?> {
        return databaseConnectable.connection {
            // swiftlint:disable:next first_where
            try profile.socialIdentifiers.query(on: $0)
                .filter(\.socialNetworkTypeID == SocialNetworkType.facebook.requireID())
                .first()
        }
    }

    func firebaseIdentifier(for profile: Profile) -> EventLoopFuture<FirebaseIdentifier> {
        return databaseConnectable.connection { connection in
            try profile.firebaseIDs.query(on: connection)
                .first()
                .map {
                    guard let firebaseIdentifier = $0 else {
                        throw ProfileStoreError.notFoundFirebaseIdentifier(try profile.requireID())
                    }

                    return firebaseIdentifier
                }
        }
    }

    func profileType(for profile: Profile) -> EventLoopFuture<ProfileType> {
        return databaseConnectable.connection { profile.profileType.get(on: $0) }
    }

    func avatarType(for profile: Profile) -> EventLoopFuture<AvatarType> {
        return databaseConnectable.connection { profile.avatarType.get(on: $0) }
    }

    // swiftlint:disable:next function_body_length
    func followers(for id: Profile.ID, andFor requestProfileID: Profile.ID) -> Future<[Profile.StoreModel]> {
        return databaseConnectable.connection { conn in
            let sql = """
                SELECT Profile.id,
                       Profile.name,
                       Profile.username,
                       ProfileType.id   as typeID,
                       ProfileType.name as typeName,
                       Profile.avatarID,
                       Profile.createdDate,
                       Profile.deleted,

                       AvatarType.name  as avatarType,

                       (
                           SELECT FI.firebaseID
                           FROM FirebaseIdentifier as FI
                           WHERE FI.profileID = Profile.id
                           LIMIT 1
                       ) as firebaseIdentifier,

                       PSI.identifier,
                       PSI.socialNetworkTypeID

                FROM Follower
                INNER JOIN Profile ON Profile.id = Follower.followerProfileID
                INNER JOIN AvatarType ON AvatarType.id = Profile.avatarID
                INNER JOIN ProfileType ON ProfileType.id = Profile.typeID
                LEFT JOIN (
                    SELECT *
                    FROM ProfileSocialIdentifier
                    GROUP BY ProfileSocialIdentifier.profileID, ProfileSocialIdentifier.socialNetworkTypeID
                ) as PSI ON PSI.profileID = Profile.id and PSI.socialNetworkTypeID = AvatarType.id


                WHERE Follower.followingProfileID = ?
                  AND Profile.deleted = 0
                  AND Follower.followerProfileID NOT IN (
                    SELECT BlockedProfile.blockedProfileID
                        FROM BlockedProfile
                        WHERE BlockedProfile.profileID = ?
                    UNION
                    SELECT BlockedProfile.profileID
                        FROM BlockedProfile
                        WHERE BlockedProfile.blockedProfileID = ?
                );
            """
            return conn.raw(sql)
                .binds([id, requestProfileID, requestProfileID])
                .all(decoding: Profile.StoreModel.self)
                .map { $0 }
        }
    }

    // swiftlint:disable:next function_body_length
    func following(for id: Profile.ID, andFor requestProfileID: Profile.ID) -> Future<[Profile.StoreModel]> {
        return databaseConnectable.connection { conn in
            let sql = """
                SELECT Profile.id,
                       Profile.name,
                       Profile.username,
                       ProfileType.id   as typeID,
                       ProfileType.name as typeName,
                       Profile.avatarID,
                       Profile.createdDate,
                       Profile.deleted,

                       AvatarType.name  as avatarType,

                       (
                           SELECT FI.firebaseID
                           FROM FirebaseIdentifier as FI
                           WHERE FI.profileID = Profile.id
                           LIMIT 1
                       ) as firebaseIdentifier,

                       PSI.identifier,
                       PSI.socialNetworkTypeID

                FROM Follower
                INNER JOIN Profile ON Profile.id = Follower.followingProfileID
                INNER JOIN AvatarType ON AvatarType.id = Profile.avatarID
                INNER JOIN ProfileType ON ProfileType.id = Profile.typeID
                LEFT JOIN (
                    SELECT *
                    FROM ProfileSocialIdentifier
                    GROUP BY ProfileSocialIdentifier.profileID, ProfileSocialIdentifier.socialNetworkTypeID
                ) as PSI ON PSI.profileID = Profile.id and PSI.socialNetworkTypeID = AvatarType.id

                WHERE Follower.followerProfileID = ?
                    AND Profile.deleted = 0
                    AND Follower.followingProfileID NOT IN (
                        SELECT BlockedProfile.blockedProfileID
                            FROM BlockedProfile
                            WHERE BlockedProfile.profileID = ?
                        UNION
                        SELECT BlockedProfile.profileID
                            FROM BlockedProfile
                            WHERE BlockedProfile.blockedProfileID = ?
                    );
            """
            return conn.raw(sql)
            .binds([id, requestProfileID, requestProfileID])
            .all(decoding: Profile.StoreModel.self)
            .map { $0 }
        }
    }

    func startFollowing(follower: Follower) -> Future<Follower> {
        return databaseConnectable.connection { conn in
        // swiftlint:disable:next first_where
        Profile.query(on: conn)
            .filter(\.id == follower.followingProfileID)
            .filter(\.deleted == false)
            .first()
            .flatMap {_ -> Future<Follower> in
                follower.save(on: conn)
            }
        }
    }

    func unfollow(follower: Follower) -> Future<Void> {
        return databaseConnectable.connection {
            Follower.query(on: $0)
                    .filter(\Follower.followerProfileID == follower.followerProfileID)
                    .filter(\Follower.followingProfileID == follower.followingProfileID)
                    .delete()
        }
    }

    func isFollowing(from fromProfileID: Profile.ID, to toProfileID: Profile.ID) -> EventLoopFuture<Bool> {
        return databaseConnectable.connection {
            let sql = """
            SELECT * FROM Follower
            INNER JOIN Profile as P1 ON P1.id = Follower.followingProfileID
            INNER JOIN Profile as P2 ON P2.id = Follower.followerProfileID
            WHERE Follower.followerProfileID = ?
                    AND Follower.followingProfileID = ?
                    AND P1.deleted = 0
                    AND P2.deleted = 0
            """
            return $0.raw(sql)
                .binds([fromProfileID, toProfileID])
                .all(decoding: Follower.self)
                .map { !$0.isEmpty }
        }
    }

    func followingTotal(profileID: Profile.ID) -> EventLoopFuture<Int> {
        // Profiles that profile is following...
        return databaseConnectable.connection { conn in
            let sql = """
            SELECT COUNT(*) as total
            FROM Follower
            WHERE Follower.`followerProfileID` = ?
                AND Follower.`followingProfileID` NOT IN (
                SELECT BlockedProfile.`blockedProfileID`
            FROM BlockedProfile
            INNER JOIN Profile as P1 ON P1.`id` = BlockedProfile.`profileID`
            INNER JOIN Profile as P2 ON P2.`id` = BlockedProfile.`blockedProfileID`
            WHERE BlockedProfile.profileID = ?
                and P1.`deleted`=0
                and P2.`deleted`=0
                )
            """
            return conn.raw(sql)
                .binds([profileID, profileID])
                .first(decoding: IntRow.self)
                .map { ($0?.total ?? 0) }
        }
    }

    func followersTotal(profileID: Profile.ID) -> EventLoopFuture<Int> {
        return databaseConnectable.connection { conn in
            let sql = """
            SELECT COUNT(*) as total
            FROM Follower
            WHERE Follower.`followingProfileID` = ?
                AND Follower.`followerProfileID` NOT IN (
                SELECT BlockedProfile.`blockedProfileID`
            FROM BlockedProfile
            INNER JOIN Profile as P1 ON P1.`id` = BlockedProfile.`profileID`
            INNER JOIN Profile as P2 ON P2.`id` = BlockedProfile.`blockedProfileID`
            WHERE BlockedProfile.profileID = ?
                and P1.`deleted`=0
                and P2.`deleted`=0
                )
            """
            return conn.raw(sql)
                .binds([profileID, profileID])
                .first(decoding: IntRow.self)
                .map { ($0?.total ?? 0) }
        }
    }

    // swiftlint:disable:next function_body_length
    func friends(for id: Profile.ID) -> Future<[Profile.StoreModel]> {
        return databaseConnectable.connection { conn in
            conn.raw("""
                SELECT DISTINCT Profile.id,
                    Profile.name,
                    Profile.username,
                    ProfileType.id as typeID,
                    ProfileType.name as typeName,
                    Profile.avatarID,
                    Profile.createdDate,
                    Profile.deleted,

                    AvatarType.name as avatarType,
                    ProfileType.name as profileType,
                    (
                        SELECT FI.firebaseID
                        FROM FirebaseIdentifier as FI
                        WHERE FI.profileID = Profile.id
                        LIMIT 1
                    ) as firebaseIdentifier,
                    PSI.identifier,
                    PSI.socialNetworkTypeID
                FROM Profile
                INNER JOIN AvatarType ON AvatarType.`id` = Profile.`avatarID`
                INNER JOIN `ProfileType` ON ProfileType.`id` = Profile.`typeID`
                INNER JOIN Follower as F1 ON Profile.`id` = F1.`followerProfileID`
                LEFT JOIN (
                        SELECT *
                        FROM ProfileSocialIdentifier
                        GROUP BY ProfileSocialIdentifier.profileID, ProfileSocialIdentifier.socialNetworkTypeID
                    ) as PSI ON PSI.profileID = Profile.id and PSI.socialNetworkTypeID = AvatarType.id
                    WHERE Profile.deleted = 0
                    AND F1.`followerProfileID` IN (
                        SELECT F2.`followingProfileID` FROM Follower as F2
                        WHERE F2.followerProfileID=?
                    )

                    AND Profile.id NOT IN (
                        SELECT BP.blockedProfileID
                            FROM BlockedProfile as BP
                            WHERE BP.profileID=?

                        UNION

                        SELECT BP.profileID
                            FROM BlockedProfile as BP
                            WHERE BP.blockedProfileID=?
                    );
            """)
            .binds([id, id, id])
            .all(decoding: Profile.StoreModel.self)
        }
    }

    func blocked(for id: Profile.ID) -> Future<[Profile.StoreModel]> {
        return databaseConnectable.connection {
            let sql = """
                SELECT Profile.id,
                Profile.name,
                Profile.username,
                ProfileType.id as typeID,
                ProfileType.`name` as typeName,
                Profile.avatarID,
                Profile.createdDate,
                Profile.deleted,

                `AvatarType`.`name` as avatarType,
                ProfileType.`name` as profileType,

                (
                    SELECT FI.firebaseID
                    FROM FirebaseIdentifier as FI
                    WHERE FI.profileID = Profile.id
                    LIMIT 1
                ) as firebaseIdentifier

                FROM BlockedProfile
                INNER JOIN Profile ON Profile.`id`=BlockedProfile.blockedProfileID
                INNER JOIN AvatarType ON AvatarType.`id` = Profile.`avatarID`
                INNER JOIN `ProfileType` ON ProfileType.`id` = Profile.`typeID`
                WHERE BlockedProfile.`profileID` = ?
                    AND Profile.`deleted` = 0
            """
            return $0.raw(sql)
                .binds([id])
                .all(decoding: Profile.StoreModel.self)
                .map { $0 }
        }
    }

    func blockProfile(blockedProfile: BlockedProfile) -> Future<BlockedProfile> {
        return databaseConnectable.transaction { connection in
            // swiftlint:disable:next first_where
            Profile.query(on: connection)
                .filter(\.id == blockedProfile.blockedProfileID)
                .filter(\.deleted == false)
                .first()
                .flatMap {_ -> Future<BlockedProfile> in

                    // Profile A block Profile B
                    return blockedProfile.save(on: connection)
                        // Profile A unfollow Profile B
                        .flatMap { profile in
                            Follower.query(on: connection)
                                    .filter(\.followerProfileID == blockedProfile.profileID)
                                    .filter(\.followingProfileID == blockedProfile.blockedProfileID)
                                    .delete()
                                    .map { profile }
                        }
                        // TODO: I'm not sure about this... This is Business Logic... Talk with team...
                        // Profile B unfollow Profile A
                        .flatMap { profile in
                            Follower.query(on: connection)
                                    .filter(\.followerProfileID == blockedProfile.blockedProfileID)
                                    .filter(\.followingProfileID == blockedProfile.profileID)
                                    .delete()
                                    .map { profile }
                        }

                }
        }
    }

    func unblockProfile(blockedProfile: BlockedProfile) -> Future<Void> {
        return databaseConnectable.connection {
            BlockedProfile.query(on: $0)
                .filter(\BlockedProfile.profileID == blockedProfile.profileID)
                .filter(\BlockedProfile.blockedProfileID == blockedProfile.blockedProfileID)
                .delete()
        }
    }

    func firebaseIdentifier(for profileID: Profile.ID) -> EventLoopFuture<FirebaseIdentifier> {
        return databaseConnectable.connection {
            // swiftlint:disable:next first_where
            FirebaseIdentifier.query(on: $0)
                .filter(\.profileID == profileID)
                .first()
                .map {
                    guard let firebaseIdentifier = $0 else {
                        throw ProfileStoreError.notFoundFirebaseIdentifier(profileID)
                    }

                    return firebaseIdentifier
                }
        }
    }

} //swiftlint:disable:this file_length
