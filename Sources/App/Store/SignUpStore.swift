import Fluent
import Vapor
import YouClap

protocol SignUpStore {
    func signup(user: User,
                createProfile: @escaping () throws -> Profile,
                facebookID: String?,
                firebaseIdentifier: String) -> EventLoopFuture<(User, Profile)>
}

extension Store: SignUpStore {
    func signup(user: User,
                createProfile: @escaping () throws -> Profile,
                facebookID: String?,
                firebaseIdentifier: String) -> EventLoopFuture<(User, Profile)> {
        return databaseConnectable.transaction { connection in
            self.create(user, withAuthenticationID: firebaseIdentifier, on: connection)
            .flatMap { user -> Future<(User, Profile)> in
                let profile = try createProfile()
                return self.create(profile: profile, for: user, firebaseIdentifier: firebaseIdentifier, on: connection)
                    .map { (user, $0) }
            }
            .flatMap { user, profile -> Future<(User, Profile)> in
                guard let facebookID = facebookID else {
                    return connection.eventLoop.future((user, profile))
                }

                let socialNetworkType = SocialNetworkType.facebook

                let userSocialIdentifier = UserSocialIdentifier(
                    userID: try user.requireID(),
                    socialNetworkTypeID: try socialNetworkType.requireID(),
                    identifier: facebookID)

                let profileSocialIdentifier = ProfileSocialIdentifier(
                    profileID: try profile.requireID(),
                    socialNetworkTypeID: try socialNetworkType.requireID(),
                    identifier: facebookID)

                return userSocialIdentifier.save(on: connection)
                    .flatMap { _ in profileSocialIdentifier.save(on: connection) }
                    .map { _ in (user, profile) }
            }
        }
    }
}
