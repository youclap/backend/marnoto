import Vapor
import YouClap

/// Register your application's routes here.
func routes(_ router: Router, _ container: Container) throws {
    try router.register(collection: HealthCheckController())

    let profileService = try container.make(ProfileServiceRepresentable.self)
    try router.register(collection: ProfileHTTPController(service: profileService))

    let userService = try container.make(UserServiceRepresentable.self)
    try router.register(collection: UserHTTPController(service: userService))

    let mexicoService = try container.make(MexicoServiceRepresentable.self)
    try router.register(collection: MexicoHTTPController(service: mexicoService))

    let signUpService = try container.make(SignUpServiceRepresentable.self)
    try router.register(collection: SignUpHTTPController(service: signUpService))
}
