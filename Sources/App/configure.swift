import FluentMySQL
import GoogleCloudKit
import GoogleCloudPubSubKit
import Vapor
import YouClap

// Called before your application initializes.
// swiftlint:disable:next function_body_length
public func configure(_ env: Environment) throws -> (Config, Services) {
    let config = Config.default()
    var services = Services.default()

    services.register(contentConfig())

    try services.register(FluentMySQLProvider())

    try services.register(GoogleCloudPubSubProvider())

    // Register middleware
    var middlewares = MiddlewareConfig() // Create _empty_ middleware config
    middlewares.use(CORSMiddleware(configuration: corsConfiguration()))
    middlewares.use(ErrorMiddleware.self) // Catches errors and converts to HTTP response
    services.register(middlewares)

    services.register { container -> DatabasesConfig in
        let logger = try container.make(Logger.self)

        guard let mysqlDatabaseConfig = MySQLDatabaseConfig.loadFromEnvironment() else {
            throw ConfigurationError.database("💥 missing required arguments")
        }

        logger.info("database configuration: \(mysqlDatabaseConfig)")

        var databaseConfig = DatabasesConfig()
        databaseConfig.enableLogging(on: .mysql)
        databaseConfig.add(database: MySQLDatabase(config: mysqlDatabaseConfig), as: .mysql)

        return databaseConfig
    }

    services.register { container -> DatabaseConnectionPoolConfig in
        let logger = try container.make(Logger.self)

        logger.info("database connections: \(Environment.get("DATABASE_CONNECTIONS") ?? "no connections")")

        guard
            let databaseMaxConnectionsAsString = Environment.get("DATABASE_CONNECTIONS"),
            let databaseMaxConnections = Int(databaseMaxConnectionsAsString)
        else { throw ConfigurationError.database("💥 missing number of connections") }

        return DatabaseConnectionPoolConfig(maxConnections: databaseMaxConnections)
    }

    guard let elasticSearchConfig = ElasticSearchConfig.loadFromEnvironment() else {
        throw ConfigurationError.elasticSearch("💥 missing required arguments")
    }

    services.register(GoogleCloudCredentialsConfiguration())
    services.register(PubSubConfiguration(scope: [.fullControl], serviceAccount: "default"))

    services.register(Store.self) { container -> Store in
        let mysqlDatabaseConnectable = MySQLDatabaseConnectable(container: container)
        let elasticSearchConnectable = ElasticSearchConnectable(container: container, config: elasticSearchConfig)
        let httpClientConnectable = HTTPClientConnectable(container: container)
        let messageQueueClient = try container.make(PublisherClient.self)

        return Store(databaseConnectable: mysqlDatabaseConnectable,
                     elasticSearchConnectable: elasticSearchConnectable,
                     httpClientConnectable: httpClientConnectable,
                     messageQueueClient: messageQueueClient)
    }

    services.register(AmericaServiceRepresentable.self) { container -> AmericaService<Store> in
        let store = try container.make(Store.self)
        let logger = try container.make(Logger.self)
        return AmericaService(store: store, logger: logger)
    }

    services.register(ProfileServiceRepresentable.self) { container -> ProfileService<Store> in
        let store = try container.make(Store.self)
        let americaService = try container.make(AmericaServiceRepresentable.self)
        let logger = try container.make(Logger.self)
        return ProfileService(store: store, americaService: americaService, logger: logger)
    }

    services.register(UserServiceRepresentable.self) { container -> UserService<Store> in
        let store = try container.make(Store.self)
        return UserService(store: store)
    }

    services.register(MexicoServiceRepresentable.self) { container -> MexicoService<Store> in
        let store = try container.make(Store.self)
        let logger = try container.make(Logger.self)
        return MexicoService(store: store, logger: logger)
    }

    services.register(SignUpServiceRepresentable.self) { container -> SignUpService<Store> in
        let store = try container.make(Store.self)
        let americaService = try container.make(AmericaServiceRepresentable.self)
        let logger = try container.make(Logger.self)
        return SignUpService(store: store,
                             americaService: americaService,
                             eventLoop: container.eventLoop,
                             logger: logger)
    }

    // Register routes to the router
    services.register(Router.self) { container -> EngineRouter in
        let router = EngineRouter.default()
        try routes(router, container)
        return router
    }

    return (config, services)
}

enum ConfigurationError: Error {
    case database(String)
    case elasticSearch(String)
}

private func contentConfig() -> ContentConfig {
    var config = ContentConfig()

    // json
    let encoder = JSONEncoder()
    let decoder = JSONDecoder()

    encoder.dateEncodingStrategy = .formatted(.iso8601)
    decoder.dateDecodingStrategy = .formatted(.iso8601)

    config.use(encoder: encoder, for: .json)
    config.use(decoder: decoder, for: .json)

    config.use(decoder: URLEncodedFormDecoder(), for: .urlEncodedForm)

    return config
}

private func corsConfiguration() -> CORSMiddleware.Configuration {
    return CORSMiddleware.Configuration(
        allowedOrigin: .all,
        allowedMethods: [.GET, .POST, .PUT, .OPTIONS, .DELETE, .PATCH],
        allowedHeaders: [
            .authToken,
            .contentType,
            .userAgent,
            .accessControlAllowOrigin
        ]
    )
}

extension HTTPHeaderName {
    static let authToken = HTTPHeaderName("Auth-Token")
}
